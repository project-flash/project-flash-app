﻿<!-- StageTest 000 NPC0-->
> echo "NPC1にはなしかけました。"

<!-- ウィンドウを表示-->

> win_open

<!-- 背景表示 -->
> show_bg "2d_bg_000_test1" 500

<!-- ノベル命令(ノベル文法記述) 上記記述に展開される-->
<!-- 名前は-で始まり、普通の文は「」で囲まれる-->

- null
背景のクロスフェードテストです。」

<!-- クロスフェードテスト -->
> show_bg "2d_bg_000_test2" 500
> wait 500
> show_bg "2d_bg_000_test3" 500
> wait 500
> show_bg "2d_bg_000_test1" 500
> wait 500

- null
終了します。」

> hide_bg 500

<!-- ウィンドウを閉じる-->
> win_close
