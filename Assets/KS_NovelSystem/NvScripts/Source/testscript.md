﻿<!-- 全命令テストスクリプト-->
* include "Sub1.md"

# main
<!-- include test-->
> echo "Sub1:SUB1"
> echo "Sub2:SUB2"
> echo "Sub3:SUB3"

<!-- nil 何もしない命令-->
> nil

<!-- _label ラベル命令-->
# testlabel

<!-- mov命令-->
> mov @0 999
> mov @1 @0
> mov @2 "hogehoge"

<!-- add 加算命令-->
> add @0 @0 @1

<!-- sub 減算命令-->
> sub @3 @0 @1

<!-- mul 乗算命令-->
> mul @4 2 3

<!-- div 余算命令-->
> div @5 128 2

<!-- echo 命令-->
> echo "echoテスト"
> echo "引数テスト：%0,%1,%2" @0 @1 @2
> echo "レジスタテスト：@pc,@sp,@sg,@tm0,@ra,@0"

<!-- comp 命令-->
> comp @1 999
> comp @1 1000
> comp @1 998

<!-- jmp 命令-->
> jmp lb_jmp
> echo "ここは無視される命令"
# lb_jmp
> echo "jmp命令"

<!-- jzer 命令-->
> comp 0 0
> jzer lb_jzer
> echo "ここは無視される命令"
# lb_jzer
> echo "jzer命令"

<!-- jmin 命令-->
> comp 0 1
> jmin lb_min
> echo "ここは無視される命令"
# lb_min
> echo "jmin命令"

<!-- jplu 命令-->
> comp 1 0
> jplu lb_plu
> echo "ここは無視される命令"
# lb_plu
> echo "jplu命令"

<!-- push 命令-->
> push @0
> push 123
> push "hogehoge"

<!-- pop 命令-->
> pop @6
> pop @7
> pop @8

<!-- call 命令(ネイティブ呼び出し)-->
> push "hogehoge"
> push 234
> call Sub1
> pop @9
> pop @10

<!-- call 命令(ラベルベース呼び出し) 上記と同様のコードに展開される-->
> Sub1 234 "hogehoge" : @9 @10

<!-- mset 命令-->
> mset 0 123

<!-- mget 命令-->
> mget @11 0

<!-- 背景表示 -->
> show_bg "2d_bg_000_test1" 500

<!-- クロスフェードテスト -->
> show_bg "2d_bg_000_test2" 500
> show_bg "2d_bg_000_test3" 500
> show_bg "2d_bg_000_test1" 500

<!-- new 命令-->
> new "2d_stand_000_firoA" "" @12
> new "2d_stand_001_goroA" "" @13

<!-- show 命令
 show インスタンス番号 x座標 y座標 重ね順 時間-->
> show @12 -200 0 1 500
> show @13 200 0 0 500

<!-- hide 命令
 hide インスタンス番号 時間-->
> hide @12 500
> hide @13 500
> wait 1000
> show @12 500
> show @13 500

<!-- scale 命令
 scale インスタンス番号 x倍率 y倍率 拡大時間-->
> scale @12 200 200 1000
> scale @12 50 50 1000
> scale @13 200 200 1000
> scale @13 50 50 1000

<!-- 同期命令 async,await-->
> hide @12 500
> hide @13 500

> async
> show @12 500
> show @13 500
> await

> async
> echo "async"
> scale @12 200 200 1000
> scale @13 200 200 1000
> await

> async
> echo "async"
> scale @12 50 50 1000
> scale @13 50 50 1000
> await

<!-- move 命令
 move インスタンス番号 x座標 y座標 時間-->
> move @12 -100 0 1000

<!-- rotate 命令
 rotate インスタンス番号 回転角 時間-->
> rotate @12 360 1000

<!-- fx 命令-->
> fx_fade "black" 500
> fx_clear 500

<!-- ウィンドウを表示-->
> win_open

<!-- ノベル命令(ノベル文法記述) 上記記述に展開される-->
<!-- 名前は-で始まり、普通の文は「」で囲まれる-->
- ゴロウ
君は、外の世界に出たくはないのか。」
- フィロ[v1]
別に出られなくてもいい。
ご飯出てくるし、……本、読めるし。」
- ゴロウ
まいったな……。」
おじさん、君の知り合いのお姉さんから、
君と遊んでやってくれって言われてるんだ。」
外が駄目なら……そうだ、
君、カメラは使ったことあるかい？」

<!-- ノベル命令(ネイティブ記述)-->
> _name "フィロ"
> _voice "v1"
> _text "ここがセリフ。"
> _text "ボイスも再生できる。"
> _newpage

<!-- ウィンドウを閉じる-->
> win_close

<!-- 背景消去命令 -->
> hide_bg 500

<!-- 音命令-->
> bgm "testbgm"
> wait 1000
> se "se_000_charpop"

<!-- wait 命令-->
> bgm "testbgm"
> wait 3000

<!-- bgm 停止-->
> stopbgm

<!-- delete 命令-->
> delete @12
> delete @13

<!-- asset 命令-->
> assert

<!-- exit 命令-->
> exit

# Sub1
> pop @tm0
> pop @tm1
> echo "subルーチン呼び出し"
> echo "引数１：%0" @tm0
> echo "引数２：%0" @tm1
> mov @tm2 1
> mov @tm3 2
> return @tm2 @tm3
