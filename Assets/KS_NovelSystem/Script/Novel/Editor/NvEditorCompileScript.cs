﻿using UnityEngine;
using UnityEditor;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace K5.Novel
{
    public class NvCompileScriptWindow : EditorWindow
    {
        string _CompilerFile = "";
        string _InputDir = "";
        string _OutDir = "";

        [MenuItem("KS_Script/CompileWindow")]
        static void Open()
        {
            GetWindow<NvCompileScriptWindow>("KSScript CompileWindow");
        }

        private void OnEnable()
        {
            _CompilerFile = EditorPrefs.GetString("KSScript_CompilerPath", "");
            _InputDir = EditorPrefs.GetString("KSScript_InputDir", "");
            _OutDir = EditorPrefs.GetString("KSScript_OutDir", "");
        }

        private void OnGUI()
        {
            EditorGUI.BeginChangeCheck();

            var labelstyle = new GUIStyle(GUI.skin.label);
            labelstyle.wordWrap = true;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("コンパイラ", GUILayout.Width(120.0f));
            EditorGUILayout.LabelField(_CompilerFile, labelstyle);
            if (GUILayout.Button("参照", GUILayout.Width(50.0f)))
            {
                _CompilerFile = EditorUtility.OpenFilePanel("choose compiler", Application.dataPath, "");
            }
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10.0f);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("スクリプトフォルダ",GUILayout.Width(120.0f));
            EditorGUILayout.LabelField(_InputDir, labelstyle);
            if (GUILayout.Button("参照", GUILayout.Width(50.0f)))
            {
                _InputDir = EditorUtility.OpenFolderPanel("choose directory", Application.dataPath, "");
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("出力フォルダ", GUILayout.Width(120.0f));
            EditorGUILayout.LabelField(_OutDir, labelstyle);
            if (GUILayout.Button("参照", GUILayout.Width(50.0f)))
            {
                _OutDir = EditorUtility.OpenFolderPanel("choose directory", Application.dataPath, "");
            }
            EditorGUILayout.EndHorizontal();

            if(GUILayout.Button("コンパイル開始"))
            {
                using (var proc = new Process())
                {
                    proc.StartInfo.FileName = _CompilerFile;
                    proc.StartInfo.CreateNoWindow = true;
                    proc.StartInfo.UseShellExecute = false;
                    proc.StartInfo.RedirectStandardOutput = true;
                    proc.StartInfo.StandardOutputEncoding = Encoding.GetEncoding("Shift-JIS");

                    //全ファイルをコンパイルする
                    var inputFiles = Directory.GetFiles(_InputDir);
                    foreach (var file in inputFiles)
                    {
                        if (Path.GetExtension(file) == ".md")
                        {
                            var outfile = Path.GetFileNameWithoutExtension(file);
                            proc.StartInfo.Arguments = file + " " + _OutDir + "/" + outfile + ".csv";
                            proc.Start();

                            proc.WaitForExit();

                            //出力をUnityコンソールにも出力
                            var outMes = proc.StandardOutput.ReadToEnd();
                            UnityEngine.Debug.Log(outMes);
                        }
                    }

                    AssetDatabase.Refresh();
                }
            }
       
            //値を保存しておく
            if(EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetString("KSScript_CompilerPath", _CompilerFile);
                EditorPrefs.SetString("KSScript_InputDir", _InputDir);
                EditorPrefs.SetString("KSScript_OutDir", _OutDir);
            }
        }
    }

}
