﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Koromosoft.Novel
{
    /// new
    /// インスタンス生成コマンド
    /// @param str プレハブ名
    /// @param str 親オブジェクト名
    /// @param reg ハンドルの格納先
    [NvCommand(id = 64)]
    sealed class NvCmd_New : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = "";
            string arg1 = "";
            NvRegister arg2 = null;

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
                arg1 = NvConvert.ToStr(args[1], vm.PC, 1);
                arg2 = NvConvert.ToReg(args[2], vm.PC, 2);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            arg2.Data = vm.Engine.SS_Graphics.NewPrefab(vm.PC, arg0, arg1);

            //出力
            NovelDebug.Log(vm.PC, "[opr]new [arg0]str:" + arg0.ToString() + "[arg1]str:" + arg1.ToString() + "[arg2]reg:" + arg2.Data.ToString());

            yield return null;
        }
    }

    /// delete
    /// インスタンス削除コマンド
    /// @param reg 画像のハンドル
    [NvCommand(id = 65)]
    sealed class NvCmd_Delete : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Engine.SS_Graphics.DeletePrefab((int)arg0.Data);

            //出力
            NovelDebug.Log(vm.PC, "[opr]delete [arg0]reg:" + arg0.Data.ToString());

            yield return null;
        }
    }

    /// show
    /// 画像を表示する
    /// @param reg 画像のハンドル
    /// @param num x座標
    /// @param num y座標
    /// @param num 重ね順
    /// @param num 時間
    [NvCommand(id = 66, syncEnable = true)]
    sealed class NvCmd_Show0 : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            int arg2 = 0;
            int arg3 = 0;
            int arg4 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
                arg2 = NvConvert.ToInt(args[2], vm.PC, 2);
                arg3 = NvConvert.ToInt(args[3], vm.PC, 3);
                arg4 = NvConvert.ToInt(args[4], vm.PC, 4);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.ShowPrefab(vm.PC, (int)arg0.Data, new Vector2(arg1, arg2), arg3, arg4 / 1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]show [arg0]reg:" + arg0.Data.ToString() + " [arg1]int:" + arg1.ToString()
                + "[arg2]int:" + arg2.ToString() + "[arg3]int:" + arg3.ToString() + "[arg4]int:" + arg4.ToString());

            yield return null;
        }
    }

    /// show
    /// 画像を表示する
    /// @param reg 画像のハンドル
    /// @param num 時間
    [NvCommand(id = 67, syncEnable = true)]
    sealed class NvCmd_Show1 : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.ShowPrefab(vm.PC, (int)arg0.Data, arg1 / 1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]show [arg0]reg:" + arg0.Data.ToString() + "[arg1]int:" + arg1.ToString());

            yield return null;
        }
    }

    /// hide
    /// 画像を表示する
    /// @param reg 画像のハンドル
    /// @param num 時間
    [NvCommand(id = 68, syncEnable = true)]
    sealed class NvCmd_Hide : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.HidePrefab(vm.PC, (int)arg0.Data, arg1 / 1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]hide [arg0]reg:" + arg0.Data.ToString() + "[arg1]int:" + arg1.ToString());

            yield return null;
        }
    }

    /// scale
    /// 画像を表示する
    /// @param reg 画像のハンドル
    /// @param num x倍率
    /// @param num y倍率
    /// @param num 時間
    [NvCommand(id = 69, syncEnable = true)]
    sealed class NvCmd_Scale : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            int arg2 = 0;
            int arg3 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
                arg2 = NvConvert.ToInt(args[2], vm.PC, 2);
                arg3 = NvConvert.ToInt(args[3], vm.PC, 3);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.Scale(vm.PC, (int)arg0.Data, new Vector2(arg1 / 100.0f, arg2 / 100.0f), arg3 / 1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]scale [arg0]reg:" + arg0.Data.ToString() +
                "[arg1]num:" + arg1.ToString() + "[arg2]num:" + arg2.ToString() + "[arg3]num:" + arg3.ToString());

            yield return null;
        }
    }

    /// move
    /// 画像を移動する
    /// @param reg 画像のハンドル
    /// @param num x差分
    /// @param num y差分
    /// @param num 時間
    [NvCommand(id = 70, syncEnable = true)]
    sealed class NvCmd_Move : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            int arg2 = 0;
            int arg3 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
                arg2 = NvConvert.ToInt(args[2], vm.PC, 2);
                arg3 = NvConvert.ToInt(args[3], vm.PC, 3);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.Move(vm.PC, (int)arg0.Data, new Vector2(arg1, arg2), arg3 / 1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]move [arg0]reg:" + arg0.Data.ToString() +
                "[arg1]num:" + arg1.ToString() + "[arg2]num:" + arg2.ToString() +
                "[arg3]num:" + arg3.ToString());

            yield return null;
        }
    }

    /// rotate
    /// 画像を回転する
    /// @param reg 画像のハンドル
    /// @param num 回転角
    /// @param num 時間
    [NvCommand(id = 71, syncEnable = true)]
    sealed class NvCmd_Rot : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            int arg2 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
                arg2 = NvConvert.ToInt(args[2], vm.PC, 2);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.Rotate(vm.PC, (int)arg0.Data, arg1, arg2 / 1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]rotate [arg0]reg:" + arg0.Data.ToString() +
                "[arg1]num:" + arg1.ToString() + "[arg2]num:" + arg2.ToString());

            yield return null;
        }
    }

    /// fx_fade
    /// 画面をフェードする
    /// @param str 色
    /// @param num 時間
    [NvCommand(id = 72, syncEnable = true)]
    sealed class NvCmd_FX_Fade : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = null;
            int arg1 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            Debug.Log("fx_fadeはまだじっそうしてません");

            //出力
            NovelDebug.Log(vm.PC, "[opr]fx_fade [arg0]str:" + arg0 +
                "[arg1]num:" + arg1.ToString());

            yield return null;
        }
    }

    /// fx_clear
    /// 画面効果をクリアする
    /// @param num 時間
    [NvCommand(id = 73, syncEnable = true)]
    sealed class NvCmd_FX_Clear : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            Debug.Log("fx_clearはまだじっそうしてません");

            //出力
            NovelDebug.Log(vm.PC, "[opr]fx_clear [arg0]str:" + arg0.ToString());

            yield return null;
        }
    }

    /// bgm
    /// BGMを再生する
    /// @param str ファイル名
    [NvCommand(id = 74)]
    sealed class NvCmd_BGM : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = null;

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Engine.SS_Sound.PlayBGM(arg0);

            //出力
            NovelDebug.Log(vm.PC, "[opr]bgm [arg0]str:" + arg0);

            yield return null;
        }
    }

    /// se
    /// SEを再生する
    /// @param str ファイル名
    [NvCommand(id = 75)]
    sealed class NvCmd_SE : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = null;

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Engine.SS_Sound.PlaySE(arg0);

            //出力
            NovelDebug.Log(vm.PC, "[opr]se [arg0]str:" + arg0);

            yield return null;
        }
    }

    /// stopbgm
    /// BGMを停止する
    /// @param str ファイル名
    [NvCommand(id = 76)]
    sealed class NvCmd_StopBGM : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //処理
            vm.Engine.SS_Sound.StopBGM();

            //出力
            NovelDebug.Log(vm.PC, "[opr]stopbgm");

            yield return null;
        }
    }

    /// win_open
    /// windowを開く
    [NvCommand(id = 77)]
    sealed class NvCmd_WinOpen : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //処理
            vm.Engine.SS_Text.WinOpen();

            //出力
            NovelDebug.Log(vm.PC, "[opr]win_open");

            yield return null;
        }
    }

    /// win_close
    /// windowを閉じる
    [NvCommand(id = 78)]
    sealed class NvCmd_WinClose : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //処理
            vm.Engine.SS_Text.WinClose();

            //出力
            NovelDebug.Log(vm.PC, "[opr]win_close");

            yield return null;
        }
    }

    /// show_bg
    /// 背景表示コマンド
    /// @param[0] str 背景名
    /// @param[1] num 表示時間
    [NvCommand(id = 79, syncEnable = true)]
    sealed class NvCmd_ShowBG : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = "";
            int arg1 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.ShowBG(vm.PC, arg0, arg1/1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]show_bg [arg0]str:" + arg0.ToString() + "[arg1]num:" + arg1.ToString());

            yield return null;
        }
    }

    /// hide_bg
    /// 背景非表示コマンド
    /// @param[0] num 表示時間
    [NvCommand(id = 80, syncEnable = true)]
    sealed class NvCmd_HideBG : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.HideBG(arg0/1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]hide_bg [arg0]num:" + arg0.ToString());

            yield return null;
        }
    }

    /// show_location
    /// ロケーション名表示コマンド
    /// @param[0] str ロケーション名
    [NvCommand(id = 81)]
    sealed class NvCmd_ShowLocation : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = "";

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Engine.SS_Graphics.ShowLocationName(arg0);

            //出力
            NovelDebug.Log(vm.PC, "[opr]show_location [arg0]str:" + arg0.ToString());

            yield return null;
        }
    }

    /// hide_location
    /// ロケーション非表示コマンド
    [NvCommand(id = 82)]
    sealed class NvCmd_HideLocation : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {

            //処理
            vm.Engine.SS_Graphics.HideLocationName();

            //出力
            NovelDebug.Log(vm.PC, "[opr]hide_location");

            yield return null;
        }
    }


}