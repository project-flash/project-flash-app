﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Koromosoft.Novel
{
    //ノベルエンジンのサブシステム
    //Unityエンジン側の機能を仲介する役割を持ち
    //エンジン上でインスタンス化されていることが前提
    public class NvSS_Base : MonoBehaviour
    {
        protected NovelEngine _Engine = null;
        //初期化処理
        public virtual void Init(NovelEngine engine)
        {
            _Engine = engine;
        }
    }
}