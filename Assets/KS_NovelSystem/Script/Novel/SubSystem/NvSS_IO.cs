﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Koromosoft.Novel
{
    public class NvSS_IO : NvSS_Base
    {
        //デバッグ用出力機能
        public void Print(string str)
        {
            Debug.Log(str);
        }
    }
}