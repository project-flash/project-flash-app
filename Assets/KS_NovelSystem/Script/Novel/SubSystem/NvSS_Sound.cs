﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Flash;

namespace Koromosoft.Novel
{
    public class NvSS_Sound : NvSS_Base
    {

        //BGM再生
        public void PlayBGM(string path)
        {
            var game = Game.Instance;
            game.SoundManager.Play(path, SoundType.BGM);
        }

        //BGM再生停止
        public void StopBGM()
        {
            var game = Game.Instance;
            game.SoundManager.Stop(SoundType.BGM);
        }

        //SE再生
        public void PlaySE(string path)
        {
            var game = Game.Instance;
            game.SoundManager.Play(path, SoundType.SE);
        }

        //ボイス再生
        public void PlayVoice(string path)
        {
            var game = Game.Instance;
            game.SoundManager.Play(path, SoundType.Voice);

            //てきすとのぽぽぽ音を一時的に停止する
            _Engine.SS_Text.IsVoiceText = true;
        }
    }
}