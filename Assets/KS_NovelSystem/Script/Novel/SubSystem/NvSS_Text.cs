﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Flash;


namespace Koromosoft.Novel
{
    [Serializable]
    class NvNamePrefabPair
    {
        public string name = "";
        public GameObject prefab = null;
    }

    //グラフィクス関係のサブシステム
    public class NvSS_Text : NvSS_Base
    {
        //ウィンドウスキンディレクトリ
        [SerializeField]
        private string _WindowSkinPath = "Skins/";

        //テキスト速度
        [SerializeField]
        private float _TextSpeed = 10.0f;
        
        //会話スキンの親オブジェクト
        [SerializeField]
        private Transform _WindowParent = null;

        //ぽぽぽ音を使うかどうか
        [SerializeField]
        private bool _UseCharacterSE = true;
        //文字表示の際のぽぽぽ音
        [SerializeField]
        private string _ShowCharSE = "";

        //ボイス再生ありのテキストかどうか（ぽぽぽ音の判定に使う）
        public bool IsVoiceText { get; set; } = false;

        //入力街の状態かどうか
        private bool isWaitClick = false;
        //次へ行くフラグ
        private bool isNext = false;
        //新しいウィンドウに変えるかどうか
        private bool isNewWindow = true;

        //スキンリスト
        private GameObject[] _SkinList = null;
        //現在のウィンドウ
        NvNovelWindowSkin _CurrentWindow = null;

        //現在のスキップ状況
        private bool isSkip = false;

        public override void Init(NovelEngine engine)
        {
            base.Init(engine);

            if (_CurrentWindow != null) _CurrentWindow.Visible = false;
        }


        public void Start()
        {
            //全スキンを読み込む
            _SkinList = Resources.LoadAll<GameObject>(_WindowSkinPath);


            if (_SkinList.Length == 0)
                Debug.LogWarning("スキンを1つもロードできませんでした。　Resources/" + _WindowSkinPath + "を確認してください。");
            else
            {
                //現在のスキンを削除
                var children = _WindowParent.GetComponentsInChildren<NvNovelWindowSkin>();
                foreach (var c in children) Destroy(c.gameObject);

                //スキンを生成
                var inst = Instantiate(_SkinList[0]);
                inst.transform.SetParent(_WindowParent.transform, false);
                _CurrentWindow = inst.GetComponent<NvNovelWindowSkin>();
                _CurrentWindow.Visible = false;
            }
        }

        public void Update()
        {
            //if(_CurrentWindow.Visible)
            //{ 
            //    //クリックまち判定
            //    if(Input.GetButtonDown("NvClick"))
            //    {
            //        if (isWaitClick) isNext = true;
            //        else isWaitClick = true;
            //    }
            //}

            ////スキップモード切替
            //if(Input.GetButtonDown("NvSkip"))
            //{
            //    isSkip = true;
            //}
            //else
            //{
            //    isSkip = false;
            //}

        }
        
        //話者名変更
        public void ChangeName(string name)
        {
            _CurrentWindow.Name = name;
        }

        //テキスト表示
        //本ゲームは１行１コンテナの作りなので、
        //showコマンドと同時にスキンを生成して、スクロールリストにプッシュする
        public IEnumerator ShowText(string str)
        {
            //改ページのタイミングかどうか
            if (isNewWindow)
            {
                _CurrentWindow.Text = "";
                isNewWindow = false;
            }

            //前回の文字列が残っていたら、改行を入れておく
            string oldText = _CurrentWindow.Text;
            if (oldText != "")
                oldText += "\n";

            //一文字ずつ表示する
            for (int i = 0; i < str.Length; i++)
            {
                //クリックされたら全文表示の状態にする
                if (isWaitClick || isSkip) break;

                var substr = str.Substring(0, i);
                _CurrentWindow.Text = oldText + substr;

                //SE再生
                if (_UseCharacterSE && !IsVoiceText)
                {
                    var game = Game.Instance;
                    game.SoundManager.Play(_ShowCharSE, SoundType.SE);
                }

                yield return new WaitForSeconds(1.0f / _TextSpeed);
            }
            _CurrentWindow.Text = oldText + str;
            
            yield return null;
        }

        //改ページ
        public IEnumerator NewPage()
        {
            isWaitClick = true;

            //クリックまち処理   
            while (!(isNext || isSkip)) yield return null;

            isNext = false;
            isWaitClick = false;
            isNewWindow = true;
            IsVoiceText = false;

            yield return null;
        }

        //クリック処理
        //文字送りが終わっていれば次のページへ
        //そうでなければ全文字を表示
        public void Click()
        {
            if (_CurrentWindow.Visible)
            {
                if (isWaitClick) isNext = true;
                else isWaitClick = true;
            }
        }

        //スキンを変更する
        public void ChangeSkin(int id)
        {
            if(_SkinList.Length <= id)
            {
                Debug.LogWarning("ChangeSkin: id=" + id.ToString() + " idがスキンの数を超えています。");
                return;
            }

            Destroy(_CurrentWindow);
            _CurrentWindow = Instantiate(_SkinList[id]).GetComponent<NvNovelWindowSkin>();
        }

        //ウィンドウを表示する
        public void WinOpen()
        {
            _CurrentWindow.Visible=true;
        }

        //ウィンドウを閉じる
        public void WinClose()
        {
            _CurrentWindow.Visible=false;
        }

        //スキップ有効処理
        public void SetSkip(bool isEnable)
        {
            isSkip = isEnable;
        }

    }
}