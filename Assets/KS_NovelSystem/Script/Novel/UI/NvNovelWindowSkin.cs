﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Koromosoft.Novel
{
    public class NvNovelWindowSkin : MonoBehaviour
    {
        //表示非表示用のアニメータ
        [SerializeField]
        private Animator _Anim = null;

        //本テキスト
        [SerializeField]
        private Text _Text = null;

        //名前ウィンドウ
        [SerializeField]
        private Text _NameText = null;

        public bool Visible
        {
            get { return _Visible; }
            set { _Anim.SetBool("IsShow", value); _Visible = value; }
        }
        private bool _Visible = true;

        private void Awake()
        {
            Text = "";
        }

        public string Text
        {
            get { return _Text.text; }
            set { _Text.text = value; }
        }

        public string Name
        {
            get { return _NameText.text; }
            set { _NameText.text = value; }
        }

        
    }
}