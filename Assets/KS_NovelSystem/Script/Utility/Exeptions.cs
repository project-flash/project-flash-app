﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K5
{ 
    class LoadGameDataException : Exception
    {
        public LoadGameDataException(string message) : base(message) {}
    }
}
