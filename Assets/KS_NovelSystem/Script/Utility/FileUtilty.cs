﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Koromosoft
{
    public static class FileUtility
    {
        //UnityのリソースフォルダからCSVをロードする
        public static List<List<string>> LoadCSVFileFromResources(string path)
        {
            var outList = new List<List<string>>();

            var rowData = Resources.Load<TextAsset>(path);
            if(!rowData)
            {
                Debug.LogError("ファイルの読み込みに失敗しました。:" + path);
                return null;
            }

            var sr = new StringReader(rowData.text);

            while(sr.Peek() != -1)
            {
                var line = sr.ReadLine();
                var unitList = SplitComma(line);

                //空行の場合は無視
                bool isEmpty = true;
                foreach (var u in unitList) if (u != "") isEmpty = false;
                if (isEmpty) continue;

                outList.Add(unitList);
            }

            return outList;
        }

        //文字列をカンマ区切りにするが、""で囲った中は無視する
        private static List<string> SplitComma(string data)
        {
            bool inText = false; //現在文字列の中にいるかのフラグ
            var list = new List<string>();
            string part = ""; //listにaddするようのパーツ

            for(int i=0; i<data.Length; i++)
            {
                char c = data[i];
                //二重引用符が出たらトグルする
                //二重引用符自体は出力に含めるのでcontinueしない
                if (c == '\"')
                { 
                    inText = !inText;
                }
                else if(c == ',' && !inText)
                {
                    //カンマがでて、なおかつテキストの中じゃない場合はスプリットする
                    list.Add(part);
                    part = "";
                    continue;
                }

                part += c;
            }

            //最後の行は、空白文字でない場合追加する。
            if (part != "") list.Add(part);

            return list;
        }
    }
}
