﻿using System;

namespace K5
{
    public class Size2D<T>
        where T: IComparable
    {
        public Size2D(T _x, T _y) { x = _x; y = _y; }

        public T x { get; set; }
        public T y { get; set; }
    }
}


