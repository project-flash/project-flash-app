﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K5
{
    public static class TextUtility
    {
        public static string SJISToUTF8(string source)
        {
            var enc_jis = Encoding.GetEncoding("Shift_JIS");
            var enc_utf8 = Encoding.UTF8;
            var bytes_mes = Encoding.Convert(enc_jis, enc_utf8, enc_jis.GetBytes(source));
            return enc_utf8.GetString(bytes_mes);
        }
    }
}
