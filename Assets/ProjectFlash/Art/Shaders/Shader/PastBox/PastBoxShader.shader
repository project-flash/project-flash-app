﻿Shader "Custom/PastBoxShader"
{
	Properties
	{
		_BaseMap("Texture", 2D) = "white" {}
		_BaseColor("Color", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		Tags
		{ 
			"RenderType" = "Opaque"
			"Queue"="Geometry-1"
			"IgnoreProjector" = "True"
			"RenderPipeline" = "UniversalPipeline"
		}
		
		ZWrite Off
		ColorMask 0

		// 1stpass
		Pass
		{
			Name "PastBoxFront"
			Tags
			{
				"LightMode" = "PastBoxFront"
			}

			Cull Front
			Stencil
			{
				Ref 1
				Comp Always
				ZFail incrWrap
			}

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/Shaders/UnlitInput.hlsl"

			struct Attributes
			{
				float4 positionOS       : POSITION;
				float2 uv               : TEXCOORD0;
			};

			struct Varyings
			{
				float2 uv        : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			Varyings vert(Attributes input)
			{
				Varyings output = (Varyings)0;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.vertex = vertexInput.positionCS;
				output.uv = TRANSFORM_TEX(input.uv, _BaseMap);

				return output;
			}

			half4 frag(Varyings input) : SV_Target
			{
				return half4(1,0,0,1);
			}
			ENDHLSL
		}

		// 2ndpass
		Pass
		{
			Name "PastBoxBack"
			Tags
			{
				"LightMode" = "PastBoxBack"
			}

			Cull Back
			
			Stencil
			{
				Ref 1
				Comp Always
				ZFail decrSat
			}

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/Shaders/UnlitInput.hlsl"

			struct Attributes
			{
				float4 positionOS       : POSITION;
				float2 uv               : TEXCOORD0;
			};

			struct Varyings
			{
				float2 uv        : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			Varyings vert(Attributes input)
			{
				Varyings output = (Varyings)0;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.vertex = vertexInput.positionCS;
				output.uv = TRANSFORM_TEX(input.uv, _BaseMap);

				return output;
			}

			half4 frag(Varyings input) : SV_Target
			{
				return half4(0,0,0,1);
			}
			ENDHLSL
		}
		
	}
	FallBack "Hidden/InternalErrorShader"
}

