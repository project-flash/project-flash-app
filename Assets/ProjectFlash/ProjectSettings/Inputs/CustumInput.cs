// GENERATED AUTOMATICALLY FROM 'Assets/ProjectFlash/Inputs/CustumInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace Flash
{
    public class CustumInput : IInputActionCollection, IDisposable
    {
        private InputActionAsset asset;
        public CustumInput()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""CustumInput"",
    ""maps"": [
        {
            ""name"": ""System"",
            ""id"": ""762759fa-68b1-4214-aeb8-1bdbcc2dcb3d"",
            ""actions"": [
                {
                    ""name"": ""ExitApp"",
                    ""type"": ""Button"",
                    ""id"": ""02106139-6121-4c28-9e5b-518a18fd9c23"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Hold""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""6422d0f9-a02a-4116-ad86-e42f24030f9b"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ExitApp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Event"",
            ""id"": ""7bde6d92-2d15-41e0-9b28-b30f4d08c14f"",
            ""actions"": [
                {
                    ""name"": ""Enter"",
                    ""type"": ""Button"",
                    ""id"": ""9480b094-bb1e-4e08-acf4-b293866d0f84"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Skip"",
                    ""type"": ""Button"",
                    ""id"": ""3d84fee5-1f93-4c19-8298-0dd6e637ce32"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Auto"",
                    ""type"": ""Button"",
                    ""id"": ""dd9276c5-1ef9-4383-9879-06390c00a0fc"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""4a86990e-ce55-4055-832e-1c091702207e"",
                    ""path"": ""<XInputController>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Enter"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""156f9bc5-f2e4-4139-9acf-2f06484abc84"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Enter"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a7e3f713-d243-40e4-a65e-20ed4f749f7a"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Enter"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""14cd9b72-b29c-47ad-9779-c677a1b15d2e"",
                    ""path"": ""<XInputController>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Skip"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""23f2e656-7b15-4c06-a308-b8fc842ea301"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Skip"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""42be3ff2-5a43-4aaf-858e-3b60c16a3a81"",
                    ""path"": ""<XInputController>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Auto"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""794c232f-8812-4d37-847b-49725b8e0f88"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Auto"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""PlayerDefault"",
            ""id"": ""bdae27ab-be19-41b0-a608-998320898f14"",
            ""actions"": [
                {
                    ""name"": ""Walk"",
                    ""type"": ""Value"",
                    ""id"": ""91256dbd-3cd1-4db0-8c02-e900f7953205"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Run"",
                    ""type"": ""Button"",
                    ""id"": ""5c327a9c-0b57-4209-bc8a-81165c398de0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""Crouch"",
                    ""type"": ""Button"",
                    ""id"": ""94c17f87-42a3-4879-8349-b7af535ff61e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Observe"",
                    ""type"": ""Button"",
                    ""id"": ""1e378c1f-39d5-4059-afcd-bf4c7df7c253"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""c4d4bce4-3274-4c75-808f-502186d90b68"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""CameraRotation"",
                    ""type"": ""Value"",
                    ""id"": ""fa0fe48c-a166-4040-b2ca-c7a75338405f"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Intaract"",
                    ""type"": ""Button"",
                    ""id"": ""fe831703-4fc0-423d-93e2-f54e83fe9934"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""CameraMode"",
                    ""type"": ""Button"",
                    ""id"": ""6a8041b9-54c3-464b-b117-4309632296dd"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""UseItem"",
                    ""type"": ""Button"",
                    ""id"": ""6a97aa04-903b-40e8-9276-f756d9485375"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2),Hold""
                },
                {
                    ""name"": ""ChangeCamera"",
                    ""type"": ""Button"",
                    ""id"": ""dba2290b-bedb-426d-b925-481ea281a8ef"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2),Hold""
                },
                {
                    ""name"": ""ChangeWeapon"",
                    ""type"": ""Button"",
                    ""id"": ""8b2c5c6b-1c54-41fe-b538-fdac99cf633e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2),Hold""
                },
                {
                    ""name"": ""ChangeMap"",
                    ""type"": ""Button"",
                    ""id"": ""fd2b10a5-c8f4-45e7-a169-625f570d2978"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""9b31590c-c69b-4d14-b293-63131123ec3e"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Keyboard"",
                    ""id"": ""21d47b7c-2691-4680-8443-017b7254364d"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""e62e8f46-5889-417a-8916-5575282bce04"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""4644089a-76b1-4666-94b7-23caa307e2c0"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""40fae546-fe7e-44b8-b5f6-22b415905438"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""5d6c807b-625b-4d5a-9029-c5e81c76c5a1"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""e85c70d7-7283-465f-970c-2bfe6e2727db"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Run"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""be69bce5-95f6-45e8-b741-2e4dce8c7412"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Run"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7433524f-b3cf-461a-a96b-dfddedf1ea2f"",
                    ""path"": ""<XInputController>/leftStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Crouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d861af9f-d573-4e90-97eb-637e463b922c"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Crouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6d9e54ca-1b92-4b7a-92ab-eeade3404d72"",
                    ""path"": ""<XInputController>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Observe"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""17d7a2eb-bead-4747-aae9-7986b8b0800a"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Observe"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d2d39377-61d8-46f3-994c-257bfe983462"",
                    ""path"": ""<XInputController>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6404945f-c139-4be8-ad69-b944f444b6ea"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""69020a67-3d15-403d-b640-3c5b988d03fd"",
                    ""path"": ""<XInputController>/rightStick"",
                    ""interactions"": """",
                    ""processors"": ""ScaleVector2(x=10,y=10)"",
                    ""groups"": """",
                    ""action"": ""CameraRotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7890e42a-69a3-4731-80ee-833299120031"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": ""ScaleVector2(x=0.5,y=0.5)"",
                    ""groups"": """",
                    ""action"": ""CameraRotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5d548000-6f03-448a-bc6b-45c982db1c3e"",
                    ""path"": ""<XInputController>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Intaract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9b18491c-e483-41a7-9e49-2e07e7b15b9b"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Intaract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8a05c5a9-e253-4bd5-9fad-0ef01a982abf"",
                    ""path"": ""<XInputController>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraMode"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2986e018-ef0f-4185-8036-d0a344790fff"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraMode"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d11de345-37cf-4dfc-b53c-6679cd2d8241"",
                    ""path"": ""<XInputController>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UseItem"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""33536328-61ae-4980-8c05-a233689c344d"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UseItem"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c6d30a04-a1a1-4b5c-9364-2fc69fe9fcf2"",
                    ""path"": ""<XInputController>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""225f0bbe-cdb5-45ab-b133-504a87a3cd61"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1eb5146c-9688-4de4-bab2-0b0257105394"",
                    ""path"": ""<XInputController>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeWeapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""188a5090-81bd-4363-bf8a-e0cf603dbc12"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeWeapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e1e82e15-9d95-49bb-b83e-146f49542044"",
                    ""path"": ""<XInputController>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeMap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3c30420b-3fca-4049-a2ec-82248d454fd6"",
                    ""path"": ""<Keyboard>/4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeMap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""PlayerCamera"",
            ""id"": ""2d4719e5-c8b7-414b-9a9a-698113b96284"",
            ""actions"": [
                {
                    ""name"": ""Walk"",
                    ""type"": ""Value"",
                    ""id"": ""4344935a-ce96-4956-b294-347b9408ca9a"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CameraShot"",
                    ""type"": ""Button"",
                    ""id"": ""70dbde43-5de1-4224-ab43-b159a2a87d74"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""CameraRotation"",
                    ""type"": ""Value"",
                    ""id"": ""bb38163c-a46d-4a87-8f1a-4df6747a0a5c"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ZoomIn"",
                    ""type"": ""Button"",
                    ""id"": ""74569a23-76f9-4faf-9aa6-cd1a3cc2626e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""ZoomOut"",
                    ""type"": ""Button"",
                    ""id"": ""d1d1d319-9470-4bfb-9056-5c14d99a802f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""2d4206ff-0b23-47da-a52d-a51d440ef88a"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""ChangeCamera"",
                    ""type"": ""Button"",
                    ""id"": ""4b56e34f-9183-4355-8355-6e47e5908bad"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""CameraMode"",
                    ""type"": ""Button"",
                    ""id"": ""761a5f59-b690-44c8-82cd-e2e86faf3e9d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""9e35dc6e-7406-4928-99c9-f26f8f99b950"",
                    ""path"": ""<XInputController>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Keyboard"",
                    ""id"": ""e3add087-aa9b-4723-800b-943c739114e3"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""cb33d450-e367-4f1e-852e-5009b58093d8"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""50819f80-0281-42e3-8e54-f787afba9917"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""8f736332-7242-4db1-83d0-da789bff7de3"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""cb35f0eb-c49b-419b-9bc9-435f1bd32e9e"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""09987c85-7ced-4fa0-a68d-718e6d60dac9"",
                    ""path"": ""<XInputController>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraShot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4d4784fc-b934-4263-a8de-8fd391bdfd65"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraShot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f94e7df6-9612-4d74-b45e-d00fe357df47"",
                    ""path"": ""<XInputController>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraRotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""10577e02-b177-4a15-966a-748085f40074"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": ""ScaleVector2"",
                    ""groups"": """",
                    ""action"": ""CameraRotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b3ddecb6-8610-433e-84d7-8abee53231f7"",
                    ""path"": ""<XInputController>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ZoomIn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""31668ccd-e96b-48b3-a7c9-2b10a7a744cc"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ZoomIn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8e99535c-441e-424b-acfd-9eff4c876eb9"",
                    ""path"": ""<XInputController>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ZoomOut"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bb83510d-5324-4fe8-ac27-2c4555f3e61f"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ZoomOut"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b3a68cf2-4895-4352-bcb0-809aab9adc1f"",
                    ""path"": ""<XInputController>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""60d22508-b452-498f-b166-97fc34af9aa4"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""09bda300-d394-4da7-bd2b-f1cd210f72e8"",
                    ""path"": ""<XInputController>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""36610328-712b-4b2c-882a-969111892153"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a05381a7-b687-41e3-8c94-af18a14058d4"",
                    ""path"": ""<XInputController>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraMode"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""687cd993-7a5f-48bd-bab6-46c17d2e10a0"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraMode"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""PlayerBattle"",
            ""id"": ""07ea4388-16a7-43e8-a36a-5e9efcb32c17"",
            ""actions"": [
                {
                    ""name"": ""GunShot"",
                    ""type"": ""Button"",
                    ""id"": ""541eb1f4-9fc8-43d2-9f15-bd2d68f16083"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""GunHold"",
                    ""type"": ""Button"",
                    ""id"": ""0dcbef35-17aa-4c79-b977-c3759608df8b"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""GunZoom"",
                    ""type"": ""Button"",
                    ""id"": ""4eaf2f7a-7745-474e-8348-b48da33a8bb6"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Rolling"",
                    ""type"": ""Button"",
                    ""id"": ""31c4f236-d5c5-494f-9062-f5c3278d9e9b"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Punch"",
                    ""type"": ""Button"",
                    ""id"": ""ea9308a1-b8fe-4125-8376-aeb99b9dec80"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""2101d59d-9cff-405f-83d9-a8e344c0fb88"",
                    ""path"": ""<XInputController>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""GunShot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c235c37e-01d3-435e-99ff-baa0dbc98fad"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""GunShot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dfcd3ce9-74e6-4822-a3e2-7138ab3da539"",
                    ""path"": ""<XInputController>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""GunHold"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""96e98078-e1c8-4cd4-9cfb-2435838c93fb"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""GunHold"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9adead0f-9830-414e-98b0-c062add4c20d"",
                    ""path"": ""<XInputController>/rightStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""GunZoom"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6945bd3a-2fbc-4e21-add6-7c772f1e796d"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""GunZoom"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""30adbc0c-3c83-4cbc-aff1-6d52fe85493f"",
                    ""path"": ""<XInputController>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rolling"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""aeee9819-8520-4fba-aaea-565d580c3c46"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rolling"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""992f9a83-f1d6-4373-94e3-a07a98c5c446"",
                    ""path"": ""<XInputController>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Punch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6568d482-ad8e-416c-8e49-416b9875c280"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Punch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""PlayerSwim"",
            ""id"": ""0298b7fd-d2a3-451b-8fe0-e1df7ada1f07"",
            ""actions"": [
                {
                    ""name"": ""Swim"",
                    ""type"": ""Value"",
                    ""id"": ""f857b3ad-7213-4a19-8156-c3bbd8287b3a"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SwimFast"",
                    ""type"": ""Button"",
                    ""id"": ""eff824af-c411-444b-a657-9a3579ee193c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""Dive"",
                    ""type"": ""Button"",
                    ""id"": ""a77ff35c-45eb-4e2a-bcb3-29f861769887"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""a7fab206-7135-4473-aea3-98b8ad3091f8"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Intaract"",
                    ""type"": ""Button"",
                    ""id"": ""868b6d9f-0969-4531-841d-9de1f51de348"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""CameraRotation"",
                    ""type"": ""Value"",
                    ""id"": ""49351b71-e1a7-4d52-9c97-90b69797398a"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Observe"",
                    ""type"": ""Button"",
                    ""id"": ""abe108bb-3ac4-401b-81a4-fdb2c0d54966"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""26eb8d41-7412-4d47-b0ea-303988ee6f14"",
                    ""path"": ""<XInputController>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Swim"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Keyboard"",
                    ""id"": ""8084ebc5-3ae6-4cb6-9ec1-de685c8f56f0"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Swim"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""2fd4f3ef-bdd5-402c-a36e-f24e7ca4df0c"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Swim"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""19ddddf9-6ba9-48f7-a61d-254c21e22c65"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Swim"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""6395d918-2ef1-48b5-91bb-6c0ed59cbaee"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Swim"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""a58f8762-ab3b-4675-9006-edba83a4cfc1"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Swim"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""c4add87f-8f3d-4db9-855c-72c340c71494"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SwimFast"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f97ac9f5-5910-4363-b50a-46fe565317c2"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SwimFast"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""73a3b40e-0743-4068-a91b-4e3dd3421b8b"",
                    ""path"": ""<XInputController>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dive"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""065e2d7d-6613-494b-a15e-179dc2497048"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dive"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2aa08f9a-3702-4038-8d40-7bcbd29bb6be"",
                    ""path"": ""<XInputController>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5a144373-21ba-4ef9-a8c7-5e9ff91e919d"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b8be20ee-dba0-4456-b092-f460eee970d2"",
                    ""path"": ""<XInputController>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Intaract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9a66e510-dfe5-416c-a0ac-75c9adeb7e57"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Intaract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c827569f-1bbd-4c90-ad45-f7dc6853d153"",
                    ""path"": ""<XInputController>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraRotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2d129f4c-b72b-427d-ae1f-32d373844a56"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": ""ScaleVector2(x=0.2,y=0.2)"",
                    ""groups"": """",
                    ""action"": ""CameraRotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e11d1cb6-9c19-4226-8104-7fc9b22f39d6"",
                    ""path"": ""<XInputController>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Observe"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6681f4af-10cb-4a80-a622-97fbb3fd8e80"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Observe"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""PlayerRadder"",
            ""id"": ""4dbfdbba-ab3b-4bd6-bf22-a5f8592e8941"",
            ""actions"": [
                {
                    ""name"": ""UpDown"",
                    ""type"": ""Value"",
                    ""id"": ""3917096e-a147-4842-80b7-cb79f7a20ded"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""FastUpDown"",
                    ""type"": ""Button"",
                    ""id"": ""1dc1ed48-dae5-4e63-9598-db5c79531625"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""Release"",
                    ""type"": ""Button"",
                    ""id"": ""4ed4ef27-f071-48ea-8eca-268d9748b802"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""BackJump"",
                    ""type"": ""Button"",
                    ""id"": ""00220f33-0605-4bdc-9e2c-f324be14f509"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""edb15a18-d52a-4a51-aef4-6fdf8e2c29b9"",
                    ""path"": ""<XInputController>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UpDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Keyboard"",
                    ""id"": ""941d8d0e-3e7a-47d2-bd27-0a4316a67ef1"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UpDown"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""807517fa-38b4-479a-bd2f-ebb6a2c9882f"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UpDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""d004b0fe-c12c-41c7-8e55-d3344abe61cc"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UpDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""976d9d5f-3c4c-4a9f-b085-5e7bb845fd13"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UpDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""352e829b-c44f-48dc-8b57-3db78d5052f1"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UpDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""6367011f-0d05-46da-9257-be33595f8c59"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FastUpDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""79246428-1649-465f-9a3b-8d43f7c436f0"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FastUpDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""48c2e03b-fe06-44d0-aafb-769a81fd2178"",
                    ""path"": ""<XInputController>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Release"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6663dfa3-d425-4053-a240-0ec8f5ae0249"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Release"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""25f60a03-9791-4963-af13-d146d1b3bcf5"",
                    ""path"": ""<XInputController>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""BackJump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""67f6338a-1c7f-4018-9fbf-447f91d34a90"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""BackJump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""UI"",
            ""id"": ""e5e90f5d-3893-4a6b-ab5d-ace92e26025d"",
            ""actions"": [
                {
                    ""name"": ""Menu"",
                    ""type"": ""Button"",
                    ""id"": ""5a161cc4-2728-4fc8-ac75-7c51e31ed463"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""a019a1e4-c035-4fb0-ac08-334a2b661401"",
                    ""path"": ""<XInputController>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Menu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Default"",
            ""bindingGroup"": ""Default"",
            ""devices"": []
        }
    ]
}");
            // System
            m_System = asset.FindActionMap("System", throwIfNotFound: true);
            m_System_ExitApp = m_System.FindAction("ExitApp", throwIfNotFound: true);
            // Event
            m_Event = asset.FindActionMap("Event", throwIfNotFound: true);
            m_Event_Enter = m_Event.FindAction("Enter", throwIfNotFound: true);
            m_Event_Skip = m_Event.FindAction("Skip", throwIfNotFound: true);
            m_Event_Auto = m_Event.FindAction("Auto", throwIfNotFound: true);
            // PlayerDefault
            m_PlayerDefault = asset.FindActionMap("PlayerDefault", throwIfNotFound: true);
            m_PlayerDefault_Walk = m_PlayerDefault.FindAction("Walk", throwIfNotFound: true);
            m_PlayerDefault_Run = m_PlayerDefault.FindAction("Run", throwIfNotFound: true);
            m_PlayerDefault_Crouch = m_PlayerDefault.FindAction("Crouch", throwIfNotFound: true);
            m_PlayerDefault_Observe = m_PlayerDefault.FindAction("Observe", throwIfNotFound: true);
            m_PlayerDefault_Jump = m_PlayerDefault.FindAction("Jump", throwIfNotFound: true);
            m_PlayerDefault_CameraRotation = m_PlayerDefault.FindAction("CameraRotation", throwIfNotFound: true);
            m_PlayerDefault_Intaract = m_PlayerDefault.FindAction("Intaract", throwIfNotFound: true);
            m_PlayerDefault_CameraMode = m_PlayerDefault.FindAction("CameraMode", throwIfNotFound: true);
            m_PlayerDefault_UseItem = m_PlayerDefault.FindAction("UseItem", throwIfNotFound: true);
            m_PlayerDefault_ChangeCamera = m_PlayerDefault.FindAction("ChangeCamera", throwIfNotFound: true);
            m_PlayerDefault_ChangeWeapon = m_PlayerDefault.FindAction("ChangeWeapon", throwIfNotFound: true);
            m_PlayerDefault_ChangeMap = m_PlayerDefault.FindAction("ChangeMap", throwIfNotFound: true);
            // PlayerCamera
            m_PlayerCamera = asset.FindActionMap("PlayerCamera", throwIfNotFound: true);
            m_PlayerCamera_Walk = m_PlayerCamera.FindAction("Walk", throwIfNotFound: true);
            m_PlayerCamera_CameraShot = m_PlayerCamera.FindAction("CameraShot", throwIfNotFound: true);
            m_PlayerCamera_CameraRotation = m_PlayerCamera.FindAction("CameraRotation", throwIfNotFound: true);
            m_PlayerCamera_ZoomIn = m_PlayerCamera.FindAction("ZoomIn", throwIfNotFound: true);
            m_PlayerCamera_ZoomOut = m_PlayerCamera.FindAction("ZoomOut", throwIfNotFound: true);
            m_PlayerCamera_Jump = m_PlayerCamera.FindAction("Jump", throwIfNotFound: true);
            m_PlayerCamera_ChangeCamera = m_PlayerCamera.FindAction("ChangeCamera", throwIfNotFound: true);
            m_PlayerCamera_CameraMode = m_PlayerCamera.FindAction("CameraMode", throwIfNotFound: true);
            // PlayerBattle
            m_PlayerBattle = asset.FindActionMap("PlayerBattle", throwIfNotFound: true);
            m_PlayerBattle_GunShot = m_PlayerBattle.FindAction("GunShot", throwIfNotFound: true);
            m_PlayerBattle_GunHold = m_PlayerBattle.FindAction("GunHold", throwIfNotFound: true);
            m_PlayerBattle_GunZoom = m_PlayerBattle.FindAction("GunZoom", throwIfNotFound: true);
            m_PlayerBattle_Rolling = m_PlayerBattle.FindAction("Rolling", throwIfNotFound: true);
            m_PlayerBattle_Punch = m_PlayerBattle.FindAction("Punch", throwIfNotFound: true);
            // PlayerSwim
            m_PlayerSwim = asset.FindActionMap("PlayerSwim", throwIfNotFound: true);
            m_PlayerSwim_Swim = m_PlayerSwim.FindAction("Swim", throwIfNotFound: true);
            m_PlayerSwim_SwimFast = m_PlayerSwim.FindAction("SwimFast", throwIfNotFound: true);
            m_PlayerSwim_Dive = m_PlayerSwim.FindAction("Dive", throwIfNotFound: true);
            m_PlayerSwim_Jump = m_PlayerSwim.FindAction("Jump", throwIfNotFound: true);
            m_PlayerSwim_Intaract = m_PlayerSwim.FindAction("Intaract", throwIfNotFound: true);
            m_PlayerSwim_CameraRotation = m_PlayerSwim.FindAction("CameraRotation", throwIfNotFound: true);
            m_PlayerSwim_Observe = m_PlayerSwim.FindAction("Observe", throwIfNotFound: true);
            // PlayerRadder
            m_PlayerRadder = asset.FindActionMap("PlayerRadder", throwIfNotFound: true);
            m_PlayerRadder_UpDown = m_PlayerRadder.FindAction("UpDown", throwIfNotFound: true);
            m_PlayerRadder_FastUpDown = m_PlayerRadder.FindAction("FastUpDown", throwIfNotFound: true);
            m_PlayerRadder_Release = m_PlayerRadder.FindAction("Release", throwIfNotFound: true);
            m_PlayerRadder_BackJump = m_PlayerRadder.FindAction("BackJump", throwIfNotFound: true);
            // UI
            m_UI = asset.FindActionMap("UI", throwIfNotFound: true);
            m_UI_Menu = m_UI.FindAction("Menu", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // System
        private readonly InputActionMap m_System;
        private ISystemActions m_SystemActionsCallbackInterface;
        private readonly InputAction m_System_ExitApp;
        public struct SystemActions
        {
            private CustumInput m_Wrapper;
            public SystemActions(CustumInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @ExitApp => m_Wrapper.m_System_ExitApp;
            public InputActionMap Get() { return m_Wrapper.m_System; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(SystemActions set) { return set.Get(); }
            public void SetCallbacks(ISystemActions instance)
            {
                if (m_Wrapper.m_SystemActionsCallbackInterface != null)
                {
                    ExitApp.started -= m_Wrapper.m_SystemActionsCallbackInterface.OnExitApp;
                    ExitApp.performed -= m_Wrapper.m_SystemActionsCallbackInterface.OnExitApp;
                    ExitApp.canceled -= m_Wrapper.m_SystemActionsCallbackInterface.OnExitApp;
                }
                m_Wrapper.m_SystemActionsCallbackInterface = instance;
                if (instance != null)
                {
                    ExitApp.started += instance.OnExitApp;
                    ExitApp.performed += instance.OnExitApp;
                    ExitApp.canceled += instance.OnExitApp;
                }
            }
        }
        public SystemActions @System => new SystemActions(this);

        // Event
        private readonly InputActionMap m_Event;
        private IEventActions m_EventActionsCallbackInterface;
        private readonly InputAction m_Event_Enter;
        private readonly InputAction m_Event_Skip;
        private readonly InputAction m_Event_Auto;
        public struct EventActions
        {
            private CustumInput m_Wrapper;
            public EventActions(CustumInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @Enter => m_Wrapper.m_Event_Enter;
            public InputAction @Skip => m_Wrapper.m_Event_Skip;
            public InputAction @Auto => m_Wrapper.m_Event_Auto;
            public InputActionMap Get() { return m_Wrapper.m_Event; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(EventActions set) { return set.Get(); }
            public void SetCallbacks(IEventActions instance)
            {
                if (m_Wrapper.m_EventActionsCallbackInterface != null)
                {
                    Enter.started -= m_Wrapper.m_EventActionsCallbackInterface.OnEnter;
                    Enter.performed -= m_Wrapper.m_EventActionsCallbackInterface.OnEnter;
                    Enter.canceled -= m_Wrapper.m_EventActionsCallbackInterface.OnEnter;
                    Skip.started -= m_Wrapper.m_EventActionsCallbackInterface.OnSkip;
                    Skip.performed -= m_Wrapper.m_EventActionsCallbackInterface.OnSkip;
                    Skip.canceled -= m_Wrapper.m_EventActionsCallbackInterface.OnSkip;
                    Auto.started -= m_Wrapper.m_EventActionsCallbackInterface.OnAuto;
                    Auto.performed -= m_Wrapper.m_EventActionsCallbackInterface.OnAuto;
                    Auto.canceled -= m_Wrapper.m_EventActionsCallbackInterface.OnAuto;
                }
                m_Wrapper.m_EventActionsCallbackInterface = instance;
                if (instance != null)
                {
                    Enter.started += instance.OnEnter;
                    Enter.performed += instance.OnEnter;
                    Enter.canceled += instance.OnEnter;
                    Skip.started += instance.OnSkip;
                    Skip.performed += instance.OnSkip;
                    Skip.canceled += instance.OnSkip;
                    Auto.started += instance.OnAuto;
                    Auto.performed += instance.OnAuto;
                    Auto.canceled += instance.OnAuto;
                }
            }
        }
        public EventActions @Event => new EventActions(this);

        // PlayerDefault
        private readonly InputActionMap m_PlayerDefault;
        private IPlayerDefaultActions m_PlayerDefaultActionsCallbackInterface;
        private readonly InputAction m_PlayerDefault_Walk;
        private readonly InputAction m_PlayerDefault_Run;
        private readonly InputAction m_PlayerDefault_Crouch;
        private readonly InputAction m_PlayerDefault_Observe;
        private readonly InputAction m_PlayerDefault_Jump;
        private readonly InputAction m_PlayerDefault_CameraRotation;
        private readonly InputAction m_PlayerDefault_Intaract;
        private readonly InputAction m_PlayerDefault_CameraMode;
        private readonly InputAction m_PlayerDefault_UseItem;
        private readonly InputAction m_PlayerDefault_ChangeCamera;
        private readonly InputAction m_PlayerDefault_ChangeWeapon;
        private readonly InputAction m_PlayerDefault_ChangeMap;
        public struct PlayerDefaultActions
        {
            private CustumInput m_Wrapper;
            public PlayerDefaultActions(CustumInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @Walk => m_Wrapper.m_PlayerDefault_Walk;
            public InputAction @Run => m_Wrapper.m_PlayerDefault_Run;
            public InputAction @Crouch => m_Wrapper.m_PlayerDefault_Crouch;
            public InputAction @Observe => m_Wrapper.m_PlayerDefault_Observe;
            public InputAction @Jump => m_Wrapper.m_PlayerDefault_Jump;
            public InputAction @CameraRotation => m_Wrapper.m_PlayerDefault_CameraRotation;
            public InputAction @Intaract => m_Wrapper.m_PlayerDefault_Intaract;
            public InputAction @CameraMode => m_Wrapper.m_PlayerDefault_CameraMode;
            public InputAction @UseItem => m_Wrapper.m_PlayerDefault_UseItem;
            public InputAction @ChangeCamera => m_Wrapper.m_PlayerDefault_ChangeCamera;
            public InputAction @ChangeWeapon => m_Wrapper.m_PlayerDefault_ChangeWeapon;
            public InputAction @ChangeMap => m_Wrapper.m_PlayerDefault_ChangeMap;
            public InputActionMap Get() { return m_Wrapper.m_PlayerDefault; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(PlayerDefaultActions set) { return set.Get(); }
            public void SetCallbacks(IPlayerDefaultActions instance)
            {
                if (m_Wrapper.m_PlayerDefaultActionsCallbackInterface != null)
                {
                    Walk.started -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnWalk;
                    Walk.performed -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnWalk;
                    Walk.canceled -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnWalk;
                    Run.started -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnRun;
                    Run.performed -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnRun;
                    Run.canceled -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnRun;
                    Crouch.started -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnCrouch;
                    Crouch.performed -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnCrouch;
                    Crouch.canceled -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnCrouch;
                    Observe.started -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnObserve;
                    Observe.performed -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnObserve;
                    Observe.canceled -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnObserve;
                    Jump.started -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnJump;
                    Jump.performed -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnJump;
                    Jump.canceled -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnJump;
                    CameraRotation.started -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnCameraRotation;
                    CameraRotation.performed -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnCameraRotation;
                    CameraRotation.canceled -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnCameraRotation;
                    Intaract.started -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnIntaract;
                    Intaract.performed -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnIntaract;
                    Intaract.canceled -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnIntaract;
                    CameraMode.started -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnCameraMode;
                    CameraMode.performed -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnCameraMode;
                    CameraMode.canceled -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnCameraMode;
                    UseItem.started -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnUseItem;
                    UseItem.performed -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnUseItem;
                    UseItem.canceled -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnUseItem;
                    ChangeCamera.started -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnChangeCamera;
                    ChangeCamera.performed -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnChangeCamera;
                    ChangeCamera.canceled -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnChangeCamera;
                    ChangeWeapon.started -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnChangeWeapon;
                    ChangeWeapon.performed -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnChangeWeapon;
                    ChangeWeapon.canceled -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnChangeWeapon;
                    ChangeMap.started -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnChangeMap;
                    ChangeMap.performed -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnChangeMap;
                    ChangeMap.canceled -= m_Wrapper.m_PlayerDefaultActionsCallbackInterface.OnChangeMap;
                }
                m_Wrapper.m_PlayerDefaultActionsCallbackInterface = instance;
                if (instance != null)
                {
                    Walk.started += instance.OnWalk;
                    Walk.performed += instance.OnWalk;
                    Walk.canceled += instance.OnWalk;
                    Run.started += instance.OnRun;
                    Run.performed += instance.OnRun;
                    Run.canceled += instance.OnRun;
                    Crouch.started += instance.OnCrouch;
                    Crouch.performed += instance.OnCrouch;
                    Crouch.canceled += instance.OnCrouch;
                    Observe.started += instance.OnObserve;
                    Observe.performed += instance.OnObserve;
                    Observe.canceled += instance.OnObserve;
                    Jump.started += instance.OnJump;
                    Jump.performed += instance.OnJump;
                    Jump.canceled += instance.OnJump;
                    CameraRotation.started += instance.OnCameraRotation;
                    CameraRotation.performed += instance.OnCameraRotation;
                    CameraRotation.canceled += instance.OnCameraRotation;
                    Intaract.started += instance.OnIntaract;
                    Intaract.performed += instance.OnIntaract;
                    Intaract.canceled += instance.OnIntaract;
                    CameraMode.started += instance.OnCameraMode;
                    CameraMode.performed += instance.OnCameraMode;
                    CameraMode.canceled += instance.OnCameraMode;
                    UseItem.started += instance.OnUseItem;
                    UseItem.performed += instance.OnUseItem;
                    UseItem.canceled += instance.OnUseItem;
                    ChangeCamera.started += instance.OnChangeCamera;
                    ChangeCamera.performed += instance.OnChangeCamera;
                    ChangeCamera.canceled += instance.OnChangeCamera;
                    ChangeWeapon.started += instance.OnChangeWeapon;
                    ChangeWeapon.performed += instance.OnChangeWeapon;
                    ChangeWeapon.canceled += instance.OnChangeWeapon;
                    ChangeMap.started += instance.OnChangeMap;
                    ChangeMap.performed += instance.OnChangeMap;
                    ChangeMap.canceled += instance.OnChangeMap;
                }
            }
        }
        public PlayerDefaultActions @PlayerDefault => new PlayerDefaultActions(this);

        // PlayerCamera
        private readonly InputActionMap m_PlayerCamera;
        private IPlayerCameraActions m_PlayerCameraActionsCallbackInterface;
        private readonly InputAction m_PlayerCamera_Walk;
        private readonly InputAction m_PlayerCamera_CameraShot;
        private readonly InputAction m_PlayerCamera_CameraRotation;
        private readonly InputAction m_PlayerCamera_ZoomIn;
        private readonly InputAction m_PlayerCamera_ZoomOut;
        private readonly InputAction m_PlayerCamera_Jump;
        private readonly InputAction m_PlayerCamera_ChangeCamera;
        private readonly InputAction m_PlayerCamera_CameraMode;
        public struct PlayerCameraActions
        {
            private CustumInput m_Wrapper;
            public PlayerCameraActions(CustumInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @Walk => m_Wrapper.m_PlayerCamera_Walk;
            public InputAction @CameraShot => m_Wrapper.m_PlayerCamera_CameraShot;
            public InputAction @CameraRotation => m_Wrapper.m_PlayerCamera_CameraRotation;
            public InputAction @ZoomIn => m_Wrapper.m_PlayerCamera_ZoomIn;
            public InputAction @ZoomOut => m_Wrapper.m_PlayerCamera_ZoomOut;
            public InputAction @Jump => m_Wrapper.m_PlayerCamera_Jump;
            public InputAction @ChangeCamera => m_Wrapper.m_PlayerCamera_ChangeCamera;
            public InputAction @CameraMode => m_Wrapper.m_PlayerCamera_CameraMode;
            public InputActionMap Get() { return m_Wrapper.m_PlayerCamera; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(PlayerCameraActions set) { return set.Get(); }
            public void SetCallbacks(IPlayerCameraActions instance)
            {
                if (m_Wrapper.m_PlayerCameraActionsCallbackInterface != null)
                {
                    Walk.started -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnWalk;
                    Walk.performed -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnWalk;
                    Walk.canceled -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnWalk;
                    CameraShot.started -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnCameraShot;
                    CameraShot.performed -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnCameraShot;
                    CameraShot.canceled -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnCameraShot;
                    CameraRotation.started -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnCameraRotation;
                    CameraRotation.performed -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnCameraRotation;
                    CameraRotation.canceled -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnCameraRotation;
                    ZoomIn.started -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnZoomIn;
                    ZoomIn.performed -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnZoomIn;
                    ZoomIn.canceled -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnZoomIn;
                    ZoomOut.started -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnZoomOut;
                    ZoomOut.performed -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnZoomOut;
                    ZoomOut.canceled -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnZoomOut;
                    Jump.started -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnJump;
                    Jump.performed -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnJump;
                    Jump.canceled -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnJump;
                    ChangeCamera.started -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnChangeCamera;
                    ChangeCamera.performed -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnChangeCamera;
                    ChangeCamera.canceled -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnChangeCamera;
                    CameraMode.started -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnCameraMode;
                    CameraMode.performed -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnCameraMode;
                    CameraMode.canceled -= m_Wrapper.m_PlayerCameraActionsCallbackInterface.OnCameraMode;
                }
                m_Wrapper.m_PlayerCameraActionsCallbackInterface = instance;
                if (instance != null)
                {
                    Walk.started += instance.OnWalk;
                    Walk.performed += instance.OnWalk;
                    Walk.canceled += instance.OnWalk;
                    CameraShot.started += instance.OnCameraShot;
                    CameraShot.performed += instance.OnCameraShot;
                    CameraShot.canceled += instance.OnCameraShot;
                    CameraRotation.started += instance.OnCameraRotation;
                    CameraRotation.performed += instance.OnCameraRotation;
                    CameraRotation.canceled += instance.OnCameraRotation;
                    ZoomIn.started += instance.OnZoomIn;
                    ZoomIn.performed += instance.OnZoomIn;
                    ZoomIn.canceled += instance.OnZoomIn;
                    ZoomOut.started += instance.OnZoomOut;
                    ZoomOut.performed += instance.OnZoomOut;
                    ZoomOut.canceled += instance.OnZoomOut;
                    Jump.started += instance.OnJump;
                    Jump.performed += instance.OnJump;
                    Jump.canceled += instance.OnJump;
                    ChangeCamera.started += instance.OnChangeCamera;
                    ChangeCamera.performed += instance.OnChangeCamera;
                    ChangeCamera.canceled += instance.OnChangeCamera;
                    CameraMode.started += instance.OnCameraMode;
                    CameraMode.performed += instance.OnCameraMode;
                    CameraMode.canceled += instance.OnCameraMode;
                }
            }
        }
        public PlayerCameraActions @PlayerCamera => new PlayerCameraActions(this);

        // PlayerBattle
        private readonly InputActionMap m_PlayerBattle;
        private IPlayerBattleActions m_PlayerBattleActionsCallbackInterface;
        private readonly InputAction m_PlayerBattle_GunShot;
        private readonly InputAction m_PlayerBattle_GunHold;
        private readonly InputAction m_PlayerBattle_GunZoom;
        private readonly InputAction m_PlayerBattle_Rolling;
        private readonly InputAction m_PlayerBattle_Punch;
        public struct PlayerBattleActions
        {
            private CustumInput m_Wrapper;
            public PlayerBattleActions(CustumInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @GunShot => m_Wrapper.m_PlayerBattle_GunShot;
            public InputAction @GunHold => m_Wrapper.m_PlayerBattle_GunHold;
            public InputAction @GunZoom => m_Wrapper.m_PlayerBattle_GunZoom;
            public InputAction @Rolling => m_Wrapper.m_PlayerBattle_Rolling;
            public InputAction @Punch => m_Wrapper.m_PlayerBattle_Punch;
            public InputActionMap Get() { return m_Wrapper.m_PlayerBattle; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(PlayerBattleActions set) { return set.Get(); }
            public void SetCallbacks(IPlayerBattleActions instance)
            {
                if (m_Wrapper.m_PlayerBattleActionsCallbackInterface != null)
                {
                    GunShot.started -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnGunShot;
                    GunShot.performed -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnGunShot;
                    GunShot.canceled -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnGunShot;
                    GunHold.started -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnGunHold;
                    GunHold.performed -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnGunHold;
                    GunHold.canceled -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnGunHold;
                    GunZoom.started -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnGunZoom;
                    GunZoom.performed -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnGunZoom;
                    GunZoom.canceled -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnGunZoom;
                    Rolling.started -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnRolling;
                    Rolling.performed -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnRolling;
                    Rolling.canceled -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnRolling;
                    Punch.started -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnPunch;
                    Punch.performed -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnPunch;
                    Punch.canceled -= m_Wrapper.m_PlayerBattleActionsCallbackInterface.OnPunch;
                }
                m_Wrapper.m_PlayerBattleActionsCallbackInterface = instance;
                if (instance != null)
                {
                    GunShot.started += instance.OnGunShot;
                    GunShot.performed += instance.OnGunShot;
                    GunShot.canceled += instance.OnGunShot;
                    GunHold.started += instance.OnGunHold;
                    GunHold.performed += instance.OnGunHold;
                    GunHold.canceled += instance.OnGunHold;
                    GunZoom.started += instance.OnGunZoom;
                    GunZoom.performed += instance.OnGunZoom;
                    GunZoom.canceled += instance.OnGunZoom;
                    Rolling.started += instance.OnRolling;
                    Rolling.performed += instance.OnRolling;
                    Rolling.canceled += instance.OnRolling;
                    Punch.started += instance.OnPunch;
                    Punch.performed += instance.OnPunch;
                    Punch.canceled += instance.OnPunch;
                }
            }
        }
        public PlayerBattleActions @PlayerBattle => new PlayerBattleActions(this);

        // PlayerSwim
        private readonly InputActionMap m_PlayerSwim;
        private IPlayerSwimActions m_PlayerSwimActionsCallbackInterface;
        private readonly InputAction m_PlayerSwim_Swim;
        private readonly InputAction m_PlayerSwim_SwimFast;
        private readonly InputAction m_PlayerSwim_Dive;
        private readonly InputAction m_PlayerSwim_Jump;
        private readonly InputAction m_PlayerSwim_Intaract;
        private readonly InputAction m_PlayerSwim_CameraRotation;
        private readonly InputAction m_PlayerSwim_Observe;
        public struct PlayerSwimActions
        {
            private CustumInput m_Wrapper;
            public PlayerSwimActions(CustumInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @Swim => m_Wrapper.m_PlayerSwim_Swim;
            public InputAction @SwimFast => m_Wrapper.m_PlayerSwim_SwimFast;
            public InputAction @Dive => m_Wrapper.m_PlayerSwim_Dive;
            public InputAction @Jump => m_Wrapper.m_PlayerSwim_Jump;
            public InputAction @Intaract => m_Wrapper.m_PlayerSwim_Intaract;
            public InputAction @CameraRotation => m_Wrapper.m_PlayerSwim_CameraRotation;
            public InputAction @Observe => m_Wrapper.m_PlayerSwim_Observe;
            public InputActionMap Get() { return m_Wrapper.m_PlayerSwim; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(PlayerSwimActions set) { return set.Get(); }
            public void SetCallbacks(IPlayerSwimActions instance)
            {
                if (m_Wrapper.m_PlayerSwimActionsCallbackInterface != null)
                {
                    Swim.started -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnSwim;
                    Swim.performed -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnSwim;
                    Swim.canceled -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnSwim;
                    SwimFast.started -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnSwimFast;
                    SwimFast.performed -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnSwimFast;
                    SwimFast.canceled -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnSwimFast;
                    Dive.started -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnDive;
                    Dive.performed -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnDive;
                    Dive.canceled -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnDive;
                    Jump.started -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnJump;
                    Jump.performed -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnJump;
                    Jump.canceled -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnJump;
                    Intaract.started -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnIntaract;
                    Intaract.performed -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnIntaract;
                    Intaract.canceled -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnIntaract;
                    CameraRotation.started -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnCameraRotation;
                    CameraRotation.performed -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnCameraRotation;
                    CameraRotation.canceled -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnCameraRotation;
                    Observe.started -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnObserve;
                    Observe.performed -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnObserve;
                    Observe.canceled -= m_Wrapper.m_PlayerSwimActionsCallbackInterface.OnObserve;
                }
                m_Wrapper.m_PlayerSwimActionsCallbackInterface = instance;
                if (instance != null)
                {
                    Swim.started += instance.OnSwim;
                    Swim.performed += instance.OnSwim;
                    Swim.canceled += instance.OnSwim;
                    SwimFast.started += instance.OnSwimFast;
                    SwimFast.performed += instance.OnSwimFast;
                    SwimFast.canceled += instance.OnSwimFast;
                    Dive.started += instance.OnDive;
                    Dive.performed += instance.OnDive;
                    Dive.canceled += instance.OnDive;
                    Jump.started += instance.OnJump;
                    Jump.performed += instance.OnJump;
                    Jump.canceled += instance.OnJump;
                    Intaract.started += instance.OnIntaract;
                    Intaract.performed += instance.OnIntaract;
                    Intaract.canceled += instance.OnIntaract;
                    CameraRotation.started += instance.OnCameraRotation;
                    CameraRotation.performed += instance.OnCameraRotation;
                    CameraRotation.canceled += instance.OnCameraRotation;
                    Observe.started += instance.OnObserve;
                    Observe.performed += instance.OnObserve;
                    Observe.canceled += instance.OnObserve;
                }
            }
        }
        public PlayerSwimActions @PlayerSwim => new PlayerSwimActions(this);

        // PlayerRadder
        private readonly InputActionMap m_PlayerRadder;
        private IPlayerRadderActions m_PlayerRadderActionsCallbackInterface;
        private readonly InputAction m_PlayerRadder_UpDown;
        private readonly InputAction m_PlayerRadder_FastUpDown;
        private readonly InputAction m_PlayerRadder_Release;
        private readonly InputAction m_PlayerRadder_BackJump;
        public struct PlayerRadderActions
        {
            private CustumInput m_Wrapper;
            public PlayerRadderActions(CustumInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @UpDown => m_Wrapper.m_PlayerRadder_UpDown;
            public InputAction @FastUpDown => m_Wrapper.m_PlayerRadder_FastUpDown;
            public InputAction @Release => m_Wrapper.m_PlayerRadder_Release;
            public InputAction @BackJump => m_Wrapper.m_PlayerRadder_BackJump;
            public InputActionMap Get() { return m_Wrapper.m_PlayerRadder; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(PlayerRadderActions set) { return set.Get(); }
            public void SetCallbacks(IPlayerRadderActions instance)
            {
                if (m_Wrapper.m_PlayerRadderActionsCallbackInterface != null)
                {
                    UpDown.started -= m_Wrapper.m_PlayerRadderActionsCallbackInterface.OnUpDown;
                    UpDown.performed -= m_Wrapper.m_PlayerRadderActionsCallbackInterface.OnUpDown;
                    UpDown.canceled -= m_Wrapper.m_PlayerRadderActionsCallbackInterface.OnUpDown;
                    FastUpDown.started -= m_Wrapper.m_PlayerRadderActionsCallbackInterface.OnFastUpDown;
                    FastUpDown.performed -= m_Wrapper.m_PlayerRadderActionsCallbackInterface.OnFastUpDown;
                    FastUpDown.canceled -= m_Wrapper.m_PlayerRadderActionsCallbackInterface.OnFastUpDown;
                    Release.started -= m_Wrapper.m_PlayerRadderActionsCallbackInterface.OnRelease;
                    Release.performed -= m_Wrapper.m_PlayerRadderActionsCallbackInterface.OnRelease;
                    Release.canceled -= m_Wrapper.m_PlayerRadderActionsCallbackInterface.OnRelease;
                    BackJump.started -= m_Wrapper.m_PlayerRadderActionsCallbackInterface.OnBackJump;
                    BackJump.performed -= m_Wrapper.m_PlayerRadderActionsCallbackInterface.OnBackJump;
                    BackJump.canceled -= m_Wrapper.m_PlayerRadderActionsCallbackInterface.OnBackJump;
                }
                m_Wrapper.m_PlayerRadderActionsCallbackInterface = instance;
                if (instance != null)
                {
                    UpDown.started += instance.OnUpDown;
                    UpDown.performed += instance.OnUpDown;
                    UpDown.canceled += instance.OnUpDown;
                    FastUpDown.started += instance.OnFastUpDown;
                    FastUpDown.performed += instance.OnFastUpDown;
                    FastUpDown.canceled += instance.OnFastUpDown;
                    Release.started += instance.OnRelease;
                    Release.performed += instance.OnRelease;
                    Release.canceled += instance.OnRelease;
                    BackJump.started += instance.OnBackJump;
                    BackJump.performed += instance.OnBackJump;
                    BackJump.canceled += instance.OnBackJump;
                }
            }
        }
        public PlayerRadderActions @PlayerRadder => new PlayerRadderActions(this);

        // UI
        private readonly InputActionMap m_UI;
        private IUIActions m_UIActionsCallbackInterface;
        private readonly InputAction m_UI_Menu;
        public struct UIActions
        {
            private CustumInput m_Wrapper;
            public UIActions(CustumInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @Menu => m_Wrapper.m_UI_Menu;
            public InputActionMap Get() { return m_Wrapper.m_UI; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(UIActions set) { return set.Get(); }
            public void SetCallbacks(IUIActions instance)
            {
                if (m_Wrapper.m_UIActionsCallbackInterface != null)
                {
                    Menu.started -= m_Wrapper.m_UIActionsCallbackInterface.OnMenu;
                    Menu.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnMenu;
                    Menu.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnMenu;
                }
                m_Wrapper.m_UIActionsCallbackInterface = instance;
                if (instance != null)
                {
                    Menu.started += instance.OnMenu;
                    Menu.performed += instance.OnMenu;
                    Menu.canceled += instance.OnMenu;
                }
            }
        }
        public UIActions @UI => new UIActions(this);
        private int m_DefaultSchemeIndex = -1;
        public InputControlScheme DefaultScheme
        {
            get
            {
                if (m_DefaultSchemeIndex == -1) m_DefaultSchemeIndex = asset.FindControlSchemeIndex("Default");
                return asset.controlSchemes[m_DefaultSchemeIndex];
            }
        }
        public interface ISystemActions
        {
            void OnExitApp(InputAction.CallbackContext context);
        }
        public interface IEventActions
        {
            void OnEnter(InputAction.CallbackContext context);
            void OnSkip(InputAction.CallbackContext context);
            void OnAuto(InputAction.CallbackContext context);
        }
        public interface IPlayerDefaultActions
        {
            void OnWalk(InputAction.CallbackContext context);
            void OnRun(InputAction.CallbackContext context);
            void OnCrouch(InputAction.CallbackContext context);
            void OnObserve(InputAction.CallbackContext context);
            void OnJump(InputAction.CallbackContext context);
            void OnCameraRotation(InputAction.CallbackContext context);
            void OnIntaract(InputAction.CallbackContext context);
            void OnCameraMode(InputAction.CallbackContext context);
            void OnUseItem(InputAction.CallbackContext context);
            void OnChangeCamera(InputAction.CallbackContext context);
            void OnChangeWeapon(InputAction.CallbackContext context);
            void OnChangeMap(InputAction.CallbackContext context);
        }
        public interface IPlayerCameraActions
        {
            void OnWalk(InputAction.CallbackContext context);
            void OnCameraShot(InputAction.CallbackContext context);
            void OnCameraRotation(InputAction.CallbackContext context);
            void OnZoomIn(InputAction.CallbackContext context);
            void OnZoomOut(InputAction.CallbackContext context);
            void OnJump(InputAction.CallbackContext context);
            void OnChangeCamera(InputAction.CallbackContext context);
            void OnCameraMode(InputAction.CallbackContext context);
        }
        public interface IPlayerBattleActions
        {
            void OnGunShot(InputAction.CallbackContext context);
            void OnGunHold(InputAction.CallbackContext context);
            void OnGunZoom(InputAction.CallbackContext context);
            void OnRolling(InputAction.CallbackContext context);
            void OnPunch(InputAction.CallbackContext context);
        }
        public interface IPlayerSwimActions
        {
            void OnSwim(InputAction.CallbackContext context);
            void OnSwimFast(InputAction.CallbackContext context);
            void OnDive(InputAction.CallbackContext context);
            void OnJump(InputAction.CallbackContext context);
            void OnIntaract(InputAction.CallbackContext context);
            void OnCameraRotation(InputAction.CallbackContext context);
            void OnObserve(InputAction.CallbackContext context);
        }
        public interface IPlayerRadderActions
        {
            void OnUpDown(InputAction.CallbackContext context);
            void OnFastUpDown(InputAction.CallbackContext context);
            void OnRelease(InputAction.CallbackContext context);
            void OnBackJump(InputAction.CallbackContext context);
        }
        public interface IUIActions
        {
            void OnMenu(InputAction.CallbackContext context);
        }
    }
}
