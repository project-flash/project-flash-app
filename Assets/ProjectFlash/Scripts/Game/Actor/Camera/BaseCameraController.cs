﻿using UnityEngine;

namespace Flash
{
    class BaseCameraController : MonoBehaviour
    {

        //-----------------
        //プロパティ
        //-----------------

        //前方ベクトル(Z軸)
        public Vector3 Forword { get { return transform.forward; } }
        //X軸ベクトル
        public Vector3 Right { get { return transform.right; } }
    }
}
