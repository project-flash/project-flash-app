﻿using UnityEngine;
using System.Collections.Generic;

using Flash.Utility;

namespace Flash
{
    [RequireComponent(typeof(Camera))]
    class FirstPersonCamera : BaseCameraController
    {
        //----------------------
        // カメラの設定
        //----------------------

        //CameraのRot.xの範囲
        [SerializeField]
        private float _minPolarAngle = 5.0f;
        [SerializeField]
        private float _maxPolarAngle = 80.0f;

        //カメラの回転スピード
        [SerializeField]
        private float _cameraRotationSpeed = 10.0f;

        //------------------------
        // ステート用フィールド
        //------------------------

        //カメラへの参照
        private Camera _camera = null;

        //次のフレームで更新する角度
        private Quaternion _camRot = Quaternion.identity;

        //-------------------------
        // Property
        //-------------------------

        //前方ベクトル(Z軸)
        public Vector3 Position { get; set; }

        //向いている方向
        public Vector3 Forward { get; set; }


        protected void Awake()
        {
            _camera = GetComponent<Camera>();
        }

        // Start is called before the first frame update
        void Start()
        {
        }

        //Update
        void Update()
        {
            UpdateTransform();
        }

        //Trasnformをアップデートする
        void UpdateTransform()
        {
            //位置の更新
            _camera.transform.position = Position;
            var forward = Vector3.Scale(Forward, new Vector3(1.0f, 0.0f, 1.0f));
            _camera.transform.forward = forward;

            //回転の更新
            _camera.transform.rotation = _camera.transform.rotation * _camRot;
            _camRot = Quaternion.identity;
        }

        //カメラの回転
        public void Rotate(Vector2 anglarVelocity)
        {
            var eular = anglarVelocity * _cameraRotationSpeed * Time.deltaTime;
            var rot = Quaternion.Euler(eular.x, eular.y, 0.0f);
            _camRot = _camRot * rot;
        }

    }
}
