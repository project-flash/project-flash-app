﻿using UnityEngine;
using UnityEditor;

using Flash.Utility;

namespace Flash
{
    static class CameraGizmos
    {
#if UNITY_EDITOR
        [DrawGizmo(GizmoType.NonSelected | GizmoType.Selected)]
        private static void DrawCameraGizmos(ThirdPersonCamera camera, GizmoType type)
        {
            Gizmos.DrawWireSphere(camera.CenterPosition, 0.1f);

            if (Application.isPlaying)
                Gizmos.DrawWireSphere(camera.LookAt, camera.CameraSize);
        }
#endif
    }
}
