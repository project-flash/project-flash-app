﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flash
{
    public class PlayerCameraLookAt : MonoBehaviour
    {
        [SerializeField]
        private Player _player = null;

        [SerializeField]
        private Vector3 _posOffset = new Vector3(0.0f, 2.0f, 0.0f);

        [SerializeField]
        private float _easingSpeed = 10.0f;

        // Start is called before the first frame update
        void Start()
        {
            transform.position = _player.transform.position + _posOffset;
        }

        // Update is called once per frame
        void Update()
        {
            var target = _player.transform.position + _posOffset;
            transform.position += (target - transform.position) * Time.deltaTime * _easingSpeed;
        }
    }
}