﻿using UnityEngine;
using System.Collections.Generic;

using Flash.Utility;

namespace Flash
{
    [RequireComponent(typeof(Camera))]
    class ThirdPersonCamera : BaseCameraController
    {
        //----------------------
        // カメラの設定
        //----------------------

        // CameraのRot.xの範囲
        [SerializeField]
        private float _minPolarAngle = 30.0f;
        [SerializeField]
        private float _maxPolarAngle = 45.0f;

        //カメラの回転スピード
        [SerializeField]
        private float _cameraRotationSpeed = 10.0f;

        //カメラのリバース設定
        [SerializeField]
        private int _isReverseX = 1;
        [SerializeField]
        private int _isReverseY = 1;

        //カメラと衝突するレイヤーリスト
        [SerializeField]
        private string[] _layerList = new string[0];

        // カメラの保つ距離
        [SerializeField]
        private float _distance = 10.0f;

        // カメラの回転中心の位置。0が注視点と一致。1がカメラの位置と一致する
        [SerializeField, Range(0.0f,1.0f)]
        private float _centerSegmentPos = 0.5f;


        //------------------------
        // ステート用フィールド
        //------------------------

        // 壁との判定に使用するレイヤーマスク
        private int _layerMask = 0;

        //カメラへの参照
        private Camera _camera = null;

        //-------------------------
        // カメラロック用のフィールド
        //-------------------------

        //カメラをロック中かどうかのフラグ
        private bool _isLock = false;

        //ロック中の注視点
        private Vector3 _lookAtWhenLocking = Vector3.zero;

        //ロック中の中点の相対位置
        private Vector3 _centerPosWhenLocking = Vector3.zero;

        //-------------------------
        // Property
        //-------------------------


        //注視点オブジェクト
        public Transform Target { get; set; } = null;
        
        // 実際の注視点の座標
        public Vector3 LookAt { get; private set; } = Vector3.zero;
        
        //回転中心
        public Vector3 CenterPosition { get; private set; } = Vector3.zero;

        //カメラのサイズを取得
        public float CameraSize { get { return CameraUtility.GetNearplaneSize(_camera).x; } }


        protected void Awake()
        {

            //レイヤーマスクを取得
            _layerMask = LayerMask.GetMask(_layerList);

            _camera = GetComponent<Camera>();
        }

        // Start is called before the first frame update
        void Start()
        {
            if (!Target)
            {
                Debug.LogWarning("カメラのターゲットがセットされていません。");
                return;
            }

            InitCenterPosition();
        }

        //Update
        void FixedUpdate()
        {
            //ロック中かどうかで処理分けする
            if (_isLock)
            {
                CenterPosition = _centerPosWhenLocking;
                LookAt = _lookAtWhenLocking;
            }
            else
            {   
                //中点を更新する
                LookAt = Target.position;
                UpdateCenterPosition();
            }

            //カメラの向きと位置を計算
            var camDir = (CenterPosition - Target.position).normalized;
            var camPos = (_isLock) ? CenterPosition : CenterPosition + camDir * _distance * (1.0f - _centerSegmentPos);

            //カメラの位置を障害物との関係で修正
            var fixedLookAtPos = UpdatePosition(LookAt, camDir, camPos);
            transform.LookAt(fixedLookAtPos);

            //ロックを解除
            _isLock = false;
        }

        //CenterPositionの初期化
        void InitCenterPosition()
        {
            //minPolarAngle maxPolarAngle の平均値を初期角度とし、プレイヤーから_distance * _centersegmentposの距離で配置する
            var centerDir = Quaternion.AngleAxis((_minPolarAngle + _maxPolarAngle) / 2.0f, transform.right) * new Vector3(0.0f, 0.0f, -1.0f);
            CenterPosition = Target.position + centerDir * _distance * _centerSegmentPos;
        }

        //CenterPositionをアップデートする
        void UpdateCenterPosition()
        {
            var dir = (CenterPosition - Target.position).normalized; //プレイヤー位置から中心点へのベクトル
            var rightVec = Vector3.Cross(dir, Vector3.up);

            // カメラ後方のベクトルを計算
            var from = Vector3.Cross(rightVec, Vector3.up);
            var angle = Vector3.SignedAngle(-from, dir, rightVec);


            //角度が最大値、最小値を越えていた場合は回転して補正する。
            if (angle <= _minPolarAngle)
            {
                dir = Quaternion.AngleAxis(_minPolarAngle - angle, rightVec) * dir;
            }
            else if (angle >= _maxPolarAngle)
            {
                dir = Quaternion.AngleAxis(_maxPolarAngle - angle, rightVec) * dir;
            }
            CenterPosition = Target.position + dir * _distance * _centerSegmentPos;
        }

        //Trasnformをアップデートする
        Vector3 UpdatePosition(Vector3 lookAtPos, Vector3 camDir, Vector3 camPos)
        {
            //注視点からカメラ方向にレイを飛ばして壁を判定
            RaycastHit hit;
            var fixedLookAtPos = lookAtPos;
            var np = CameraUtility.GetNearplaneSize(_camera);
            if (Physics.SphereCast(lookAtPos, np.x, camDir, out hit, _distance, _layerMask))
            {
                camPos = lookAtPos + camDir * hit.distance;
            }

            //位置を更新
            transform.position = camPos;

            //デバグ表示
            Debug.DrawLine(transform.position, lookAtPos, Color.red);

            return fixedLookAtPos;
        }

        //注視点中心に中点を回転させる
        public void RotateCenter(float x, float y, float delta)
        {
            var rotX = x * _isReverseX * _cameraRotationSpeed * delta;
            var rotY = y * _isReverseY * _cameraRotationSpeed * delta;

            var dir = (CenterPosition - Target.position).normalized; //プレイヤー位置から中心点へのベクトル
            dir = Quaternion.AngleAxis(rotX, transform.up) * dir;
            dir = Quaternion.AngleAxis(rotY, transform.right) * dir;

            CenterPosition = Target.position + dir * _distance * _centerSegmentPos;
        }

        /// <summary>
        /// カメラの位置と方向を、特定の値にセットする
        /// Z注目機能に使用
        /// </summary>
        /// <param name="lookat"> 注視するオブジェクトなり中空なりの座標 </param>
        /// <param name="position"> カメラの位置 </param>
        public void Lock(Vector3 lookat, Vector3 position)
        {
            _lookAtWhenLocking = lookat;
            _centerPosWhenLocking = Target.position + position;
            _isLock = true;
        }

    }
}
