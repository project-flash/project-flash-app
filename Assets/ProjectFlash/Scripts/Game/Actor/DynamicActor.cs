﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Flash
{
    [RequireComponent(typeof(Rigidbody))]
    class DynamicActor : Actor, IPhysically
    {
        private Rigidbody _rig = null;
        private float _mass = 0.0f;


        void Awake()
        {
            _rig = GetComponent<Rigidbody>();
            _mass = _rig.mass;

        }

        public void MassEnable(bool isEnable)
        {
            _rig.mass = isEnable ? _mass : 0.0f;
        }
    }
}
