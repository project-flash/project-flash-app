﻿using UnityEngine;
using System.Collections.Generic;

namespace Flash
{
    /// <summary>
    /// 動く床スクリプト
    /// </summary>

    class AMoveFloor : Actor
    {
        [System.Serializable]
        public class FloorController
        {
            public Vector3 targetPosition = Vector3.zero;
            public float moveSpeed = 1.0f;
        }

        public enum REPEAT
        {
            LOOP,
            REVERSE,
            STOP
        }

        [SerializeField]
        private REPEAT _repeat = REPEAT.LOOP;

        [SerializeField]
        private List<FloorController> _floorController = new List<FloorController>();

        private int _fcIndex = 0;

        private Vector3 _moveDirection = Vector3.zero;

        private bool _bReverse = false;

        [SerializeField]
        private GameObject targetCharacter;
        private Player targetPlayerClass = null;

        private bool isCharacterGrounded = false;

        public Vector3 Velo { get; set; }

        private Rigidbody rigidbody;

        private void Awake()
        {
        }

        private void Start()
        {
            if (_floorController.Count == 0)
            {
                Debug.LogWarning("動作ルートが指定されていません");
            }
            transform.position = _floorController[_fcIndex].targetPosition;
            _fcIndex = 1;

            targetPlayerClass = targetCharacter?.GetComponentInChildren<Player>();
            rigidbody = gameObject.GetComponent<Rigidbody>();

        }

        private void Update()
        {
            if (_fcIndex > _floorController.Count - 1)
            {
                return;
            }

            var index = _fcIndex;
            if (_bReverse)
            {
                index = _floorController.Count - index - 1;
            }

            var targetPosition = _floorController[index].targetPosition;
            var speed = _floorController[index].moveSpeed;
            var nextPosition = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
            Velo = (nextPosition - transform.position) / Time.deltaTime;

            rigidbody.MovePosition(nextPosition);



            if (Vector3.Distance(transform.position, targetPosition) < 0.01f)
            {
                _fcIndex++;

                if (_fcIndex == _floorController.Count)
                {
                    switch (_repeat)
                    {
                        case REPEAT.LOOP:
                            _fcIndex = 0;
                            break;
                        case REPEAT.REVERSE:
                            _fcIndex = 0;
                            _bReverse = !_bReverse;
                            break;
                        case REPEAT.STOP:
                            break;
                    }
                }
            }
        }


        //void OnCollisionEnter(Collision other)
        //{
        //    Debug.Log("OnCollisionEnter");

        //    if (other.transform.position.y > transform.position.y)
        //    {
        //        isCharacterGrounded = true;
        //    }
        //    else
        //    {
        //        isCharacterGrounded = false;
        //    }
        //}

        //private void OnTriggerStay(Collider other)
        //{
        //    Debug.Log("OnTriggerStay");
        //    if (other.transform.position.y > transform.position.y)
        //    {
        //        isCharacterGrounded = true;
        //    }
        //    else
        //    {
        //        isCharacterGrounded = false;
        //    }


        //}

        //private void OnTriggerExit(Collider other)
        //{
        //    Debug.Log("OnTriggerExit");
        //    isCharacterGrounded = false;
        //}


        //private void OnControllerColliderHit(ControllerColliderHit hit)
        //{
        //    Debug.Log("OnControllerColliderHit");
        //}

        //private void Update()
        //{

        // TODO: 下記要件を満たす動作を作成する
        // 1. 本スクリプトがアタッチされたオブジェクトと、プレイヤーの足が設置しているとき、
        //    プレイヤーは本オブジェクトに追従して動く。
        //    ※追従して動く処理は、playerクラスの、player.AddVelocity()メソッドを活用してください。適切な速度を与えてやることで、プレイヤーは無条件でその方向に動きます。
        // 2. 本オブジェクトの動作は、インスペクターでリスト形式などで動作ルートを設定できるようにする。
        // 3. 動作ルートを通ったあとは「逆再生」「ループ再生」「その場で静止」する機能をつける。
        // 4. [EX]本オブジェクトとは別に位置情報を持つ空のゲームオブジェクト郡を作成しておき、それをターゲットとしてルートを構成するようなしくみを実装する。
        //    例えば、ゲームオブジェクト1~10を定義しておき、本オブジェクトは、1~10の各オブジェクトをたどるよう動作する
        //    ターゲットの指定は、インスペクタで行える。例えば「速度」「ターゲットのゲームオブジェクトへの参照」「イージングの種類」をもった構造体のリストなどがよいです。
        //    実装はDOTweenなどのプラグインがいれてあるので、コレを使うと良いです。
        //    イージングについては、こちらを参照。https://qiita.com/ryosebach/items/5bd90d4b464b9059f35d

        // CHECK:
        // 1. プレイヤーが本オブジェクトに乗っており、かつオブジェクトが動いているとき、
        //    プレイヤーが振り落とされない。
        // 2. 本オブジェクトがプレイヤーの上部で動作しており、
        //    プレイヤーがジャンプして本オブジェクトに頭から接触したとき、
        //    プレイヤーは本オブジェクトに追従せず、そのまま落下する。
        // 3. オブジェクトが指定した通りのルートを通っている。
        // 4. オブジェクトが指定した動作を終えたあと、「逆再生」「ループ再生」「その場で静止」いずれか指定した動作をおこなう。3パターンちゃんと確認すること。
        // 5. ルートを指定しなかった場合（未定義動作）、エラーが起きずにゲームが続行される、もしくは適切なエラーメッセージをはき、ゲームを停止する。
        //}
    }
}
