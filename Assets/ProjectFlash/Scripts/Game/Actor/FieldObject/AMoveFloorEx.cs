﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Sirenix.OdinInspector;
using DG.Tweening;

namespace Flash
{
    public enum EMovePlatformType
    {
        Loop,
        PingPong,
        Once,
        _num
    }

    /// <summary>
    /// 動く床スクリプト
    /// </summary>
    class AMoveFloorEx : Actor
    {
        //動作ルート
        [SerializeField]
        private List<Transform> _route = new List<Transform>();

        //動作速度
        [SerializeField, Range(0.0f, 10.0f)]
        private float _speed = 1.0f;

        //動作タイプ
        [SerializeField]
        private EMovePlatformType _moveType = EMovePlatformType.Loop;

        //--------------
        //内部フィールド
        //--------------
        private Dictionary<Collider, Transform> _objectList = new Dictionary<Collider, Transform>();
        private Dictionary<Collider, Player> _playerList = new Dictionary<Collider, Player>();

        //--------------
        // インスペクタでのデバッグ用
        //--------------
        [Header("------DEBUG-------")]
        [ShowInInspector, ReadOnly]
        private int _currentRoute = 0;

        private void Awake()
        {
            //移動開始
            StartCoroutine(_UpdatePos());
        }

        //位置の更新 目標地点に到達するたびに呼ばれる
        private IEnumerator _UpdatePos()
        {
            var begin = 0;//開始位置
            var iter = 1; //次のインデックスとの差分

            while (true)
            {
                int i = begin;
                do
                {
#if DEBUG
                    _currentRoute = i % _route.Count;
#endif
                    var pos = _route[i % _route.Count].position;

                    //posまで移動する
                    yield return MoveTo(pos);

                    i += iter;
                } while (i < _route.Count && i > 0);

                //周回方法を決定
                switch(_moveType)
                {
                    case EMovePlatformType.Loop:
                        begin = 0; iter = 1; break;
                    case EMovePlatformType.PingPong:
                        begin = (iter > 0) ? _route.Count : 0;
                        iter *= -1; break;
                    case EMovePlatformType.Once:
                        begin = 0; iter = 0; break;
                }
            }
        }

        // targetまでを一定のスピードで進む
        private IEnumerator MoveTo(Vector3 target)
        {
            while(true)
                    {
                var dir = target - transform.position;
                var dx = dir.normalized * Time.deltaTime * _speed;

                //posに十分近くなったら完了
                if (dir.magnitude < Time.deltaTime * _speed) break;

                //自分の位置を更新
                transform.position += dx;

                //プレイヤーの位置を更新
                foreach (var p in _playerList)
                    p.Value.Move(dx);

                yield return new WaitForEndOfFrame();
            }
        }

        public void OnTriggerEnter(Collider other)
        {
            Debug.Log("ontriggerenter from:" + this.name + " to:" + other.name);

            if (other.CompareTag("Actor"))
            {
                _objectList.Add(other, other.transform.parent);
                other.transform.SetParent(this.transform);
            }
            else if(other.CompareTag("Player"))
            {
                _playerList.Add(other, other.GetComponent<Player>());
            }
        }


        public void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Actor"))
            {
                other.transform.SetParent(_objectList[other]);
                _objectList.Remove(other);
            }
            else if(other.CompareTag("Player"))
            {
                _playerList.Remove(other);
            }
        }


    }
}
