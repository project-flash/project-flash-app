﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Flash
{
    [RequireComponent(typeof(Collider))]
    public class PastBox : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("PastBox"))
                Debug.Log("call enter in pastbox");
        }
    }
}