﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flash
{
    [RequireComponent(typeof(Collider),typeof(Rigidbody))]
    class PastObject : Actor
    {
        [SerializeField]
        private bool _defaultIsFuture = true;
        [SerializeField]
        private bool _IsPast = true;
        [SerializeField]
        private GameObject _Object = null;

        private Collider[] _ColliderList = null;

        private void Awake()
        {
            _Object.SetActive(_defaultIsFuture != _IsPast);
            _ColliderList = GetComponents<Collider>();
        }

        private void OnTriggerEnter(Collider other)
        {
            //PastBox内に入っているとき
            if (other.CompareTag("PastBox"))
            {
                _Object.SetActive(_IsPast);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("PastBox"))
            {
                _Object.SetActive(!_IsPast);
            }
        }
    }
}