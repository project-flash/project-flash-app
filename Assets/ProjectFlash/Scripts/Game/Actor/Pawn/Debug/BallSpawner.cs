﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

using Flash.Utility;

namespace Flash
{
    public class BallSpawner : MonoBehaviour
    {
        [SerializeField]
        private GameObject _ballPrefab = null;

        [SerializeField]
        private Transform _player = null;

        [SerializeField]
        private float _ballSpeed = 10.0f;

        //ボールのスポーン感覚調整用
        private const float _duration = 0.1f;
        private float _progress = 0.0f;
        private int _counter = 0;

        private CustumInput _input;

        private void Awake()
        {
            _input = Game.Instance.InputSystem;
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            _progress -= Time.deltaTime;

            if (InputUtility.IsPressed(_input.PlayerDefault.Intaract) && _progress < 0.0f)
            {
                var num_h = 5;
                var num_v = 5;
                //たまを任意の数生成する
                for (int i = 0; i < num_h; i++)
                {
                    for (int j = 0; j < num_v; j++)
                    {
                        var pos = _player.position + _player.forward.normalized;
                        pos.y += 1.0f;

                        //水平方向にずらす
                        var horizontal = Vector3.Cross(_player.forward.normalized, Vector3.up);
                        horizontal = horizontal * (i - num_h / 2.0f) / 2.0f;
                        pos += horizontal;

                        //垂直方向にずらす
                        var vertical = Vector3.up * (j - num_v / 2.0f) / 2.0f;
                        pos += vertical;

                        CreateBall(pos);
                    }
                }


                _progress = _duration;
            }

            //デバッグ表示
            Debug.Log("玉の数：" + _counter + "  推定ポリ数：" + _counter * 32);
        }

        private void CreateBall(Vector3 position)
        {
            //ボールの生成
            var inst = Instantiate(_ballPrefab);
            inst.transform.SetParent(this.transform);

            inst.transform.position = position;

            var comp = inst.GetComponent<Rigidbody>();
            comp.velocity = _player.forward.normalized * _ballSpeed;

            _counter++;
        }
    }
}