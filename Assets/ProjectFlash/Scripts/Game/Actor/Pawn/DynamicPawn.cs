﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flash
{
    /// <summary>
    /// その場を動く可能性のあるPawnオブジェクト
    /// CharacterControllerで駆動する
    /// </summary>
    [RequireComponent(typeof(CharacterController))]
    class DynamicPawn : Pawn
    {
        //----------------
        // 外部コンポーネント
        //----------------
        protected CharacterController _characterController = null;

        protected virtual void Awake()
        {
            _characterController = GetComponent<CharacterController>();
        }

    }
}