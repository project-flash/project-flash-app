﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flash
{
    class Pawn : Actor
    {
        //指定した方向に振り向く処理
        public virtual void TurnTo(Vector3 targetPos, float turnSpeed=0.5f)
        {
            var targetDir = targetPos - transform.position;
            var pawnDir = transform.forward;

            var angle = Vector3.SignedAngle(pawnDir, targetDir, Vector3.up);
            var endAngle = transform.eulerAngles + new Vector3(0.0f, angle, 0.0f);

            //回転処理
            transform.DORotate(endAngle, turnSpeed);
        }
    }
}