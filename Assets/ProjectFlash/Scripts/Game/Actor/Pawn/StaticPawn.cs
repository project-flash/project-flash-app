﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flash
{
    /// <summary>
    /// その場を動かないPawnオブジェクト
    /// その場で動かないNPC等に使用する想定
    /// CharacterControllerを保持せず、物理も働かない
    /// </summary>
    class StaticPawn : Pawn
    {
        //指定した方向に振り向く処理
        public override void TurnTo(Vector3 targetPos, float turnSpeed = 0.5f)
        {
            var targetDir = targetPos - transform.position;
            var pawnDir = transform.forward;

            var angle = Vector3.SignedAngle(pawnDir, targetDir, Vector3.up);
            var endAngle = transform.eulerAngles + new Vector3(0.0f, angle, 0.0f);

            //回転処理
            transform.DORotate(endAngle, turnSpeed);
        }
    }
}