﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Sirenix.OdinInspector;

using Flash.Utility;

namespace Flash
{

    /// <summary>
    /// 外部コンポーネントから操作を伴うPawnオブジェクト
    /// </summary>
    /// 
    class Player : DynamicPawn
    {
        //----------------------
        // 定数
        //----------------------

        //振り向き速度
        [SerializeField]
        private readonly float _turnSpeed = 10.0f;

        //重力
        [SerializeField]
        private readonly Vector3 _gravity = new Vector3(0.0f, -9.8f, 0.0f);

        //落下限界速度(物体を突き抜けない程度の)
        [SerializeField]
        private readonly float _limitSpeed = 50.0f;

        //沿って走れる坂道の限界の角度
        [SerializeField]
        private readonly float _slopeDownLimit = 45.0f;

        //接地判定のバイアス
        [SerializeField]
        private readonly float _groundCastBias = 0.3f;

        //アニメーションの最大速度時のforwordの値
        [SerializeField]
        private readonly float _animMaxForword = 2.0f;
        
        //----------------------
        // プロパティ
        //----------------------

        //移動速度
        public float MoveSpeed { get; set; } = 0.0f;

        //移動方向
        public Vector3 MoveDirection { get; set; } = Vector2.zero;

        //しゃがみ状態フラグ
        public bool IsCrouch { get; set; } = false;

        //入力で撮りうる最大の速度
        public float MaxMoveSpeed { get; set; } = 5.0f;

        //-------------------
        // 外部コンポーネント
        //-------------------
        [SerializeField]
        private Animator _animator = null;

        //-------------------
        // 内部フィールド
        //-------------------

        //処理を委譲するステートマシン
        private PlayerStateMachine _stateMachine = new PlayerStateMachine();

        //速度
        [ShowInInspector, ReadOnly]
        private Vector3 _velocity = Vector3.zero;

        //設置状態の内部フラグ
        [ShowInInspector, ReadOnly]
        private bool _onGround = false;
        private bool _lastOnGround = false;

        //追加加算される速度
        [ShowInInspector, ReadOnly]
        private Vector3 _additionalVelocity = Vector3.zero;

        //方向ロックフラグ
        //Z注目機能で使う
        private bool _isObserving = false;

        protected override void Awake()
        {
            base.Awake();

            //ステートを追加
            _stateMachine.AddState((int)EPlayerState.OnGround, new PlayerState_OnGround(_stateMachine));
            _stateMachine.AddState((int)EPlayerState.IsJumping, new PlayerState_IsJumping(_stateMachine));
            _stateMachine.AddState((int)EPlayerState.IsDamaged, new PlayerState_IsDamaged(_stateMachine));
            _stateMachine.AddState((int)EPlayerState.IsObserving, new PlayerState_IsObserving(_stateMachine));

            //遷移を追加
            _stateMachine.AddTransition((int)EPlayerState.OnGround, (int)EPlayerState.IsJumping, () => { return !_onGround; });
            _stateMachine.AddTransition((int)EPlayerState.IsJumping, (int)EPlayerState.OnGround, () => { return _onGround; });
            _stateMachine.AddTransition((int)EPlayerState.OnGround, (int)EPlayerState.IsObserving, () => { return _isObserving; });
            _stateMachine.AddTransition((int)EPlayerState.IsObserving, (int)EPlayerState.OnGround, () => { return !_isObserving; });
            _stateMachine.AddTransition((int)EPlayerState.IsObserving, (int)EPlayerState.IsJumping, () => { return !_onGround; });


            //ステートの初期化
            _stateMachine.Init((int)EPlayerState.OnGround);
            {
                _stateMachine.SlopeDownLimit = _slopeDownLimit;
                _stateMachine.Gravity = _gravity;
                _stateMachine.TurnSpeed = _turnSpeed;
                _stateMachine.Velocity = Vector3.zero;
            }
        }

        // Update is called once per frame
        void Update()
        {
            _velocity = _stateMachine.Velocity;//インスペクタでの確認用

            //接地判定を更新
            UpdateOnGround();

            //ステートにより、次フレームの速度と回転を計算
            {
                _stateMachine.PlayerPosition = transform.position;
                _stateMachine.MoveDirection = MoveDirection;
                _stateMachine.MoveSpeed = MoveSpeed;
                _stateMachine.Forward = transform.forward;
                _stateMachine.AdditionalVelocity = _additionalVelocity;
            }
            _stateMachine.Update();

            ////限界速度でフィルタしておく
            if (_stateMachine.Velocity.magnitude > _limitSpeed)
                _stateMachine.Velocity *= _limitSpeed / _stateMachine.Velocity.magnitude;

            //速度と回転を更新
            _characterController.Move(_stateMachine.Velocity * Time.deltaTime);
            transform.Rotate(_stateMachine.RotAngle);

            // アニメーションアップデート
            UpdateAnimator(MoveSpeed, _stateMachine.RotAngle.y/90.0f, _velocity.y, !_onGround);

            //1フレーム限りの値をリセット
            _additionalVelocity = Vector3.zero;
            _isObserving = false;
        }

        //ジャンプ命令を発行する
        public void Jump(float power)
        {
            // 接地時で、ジャンプ中でないときのみジャンプ命令を発行する
            if (_onGround)
            {
                _onGround = false;

                //ジャンプ力を速度に加える
                var v = _stateMachine.Velocity;
                v.y = power;
                _stateMachine.Velocity = v;
            }
        }

        //dx分だけ、キャラクターを動かす。
        public void Move(Vector3 dx)
        {
            _characterController.Move(dx);
        }

        //方向を特定の方向にロックする
        public void Lock(Vector3? lookAt)
        {
            _isObserving = true;
            _stateMachine.LookAt = lookAt;
        }

        // アニメーションのステートを更新する
        void UpdateAnimator(float speed, float rotAngle, float velocityY, bool isjumping)
        {
            // update the animator parameters
            _animator.SetFloat("Forward", speed * _animMaxForword / MaxMoveSpeed, 0.1f, Time.deltaTime);
            _animator.SetFloat("Turn", rotAngle, 0.1f, Time.deltaTime);
            _animator.SetBool("Crouch", IsCrouch);
            _animator.SetBool("Jump", isjumping);

            if (isjumping)
            {
                // 接地後にこの値を変更するとブレンドツリーでポーズが急に変わってしまうため、空中にいるときのみ変更
                _animator.SetFloat("VelocityY", velocityY);
            }
        }

        // 接地判定を更新する
        // 離陸時はonGroundを手動で更新するか、SphereCastの結果で判定
        // 着地時は、キャラクターコントローラのonGroundで取得する
        // こうすることで、ジャンプ力が微力で離陸に失敗するケースを防ぐ
        void UpdateOnGround()
        {
            _lastOnGround = _onGround;

            if (_onGround)
            {
                var r = _characterController.radius;
                var h = _characterController.height;
                var cy = _characterController.center.y;

                var offset = Vector3.up * cy + Vector3.down * (h - r * 2) / 2.0f;
                Vector3 sphereCenter = transform.position + offset;

                Physics.SphereCast(sphereCenter, r - 0.01f, Vector3.down, out RaycastHit hit);

                _onGround = hit.distance <= _groundCastBias;
            }
            //空中にいる場合はレイキャストせずに、キャラクターコントローラの当たり判定に任せる
            // バイアスを使用しないので、コライダと接触＝接地になる
            else
            {
                _onGround = _characterController.isGrounded;
            }
        }

    }
}