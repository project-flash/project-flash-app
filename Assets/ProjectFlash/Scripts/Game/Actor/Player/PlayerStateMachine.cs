﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Sirenix.OdinInspector;

using Flash.Utility;

namespace Flash
{
    enum EPlayerState : int
    {
        OnGround,
        IsJumping,
        IsDamaged,
        IsObserving
    }

    //プレイヤー用ステートマシン
    //ステートの使用する値をプレイヤークラスと同期する
    class PlayerStateMachine : StateMachine
    {
        public Vector3 PlayerPosition { get; set; } = Vector3.zero;
        public Vector3 MoveDirection { get; set; } = Vector3.zero;
        public float MoveSpeed { get; set; } = 0.0f;
        public Vector3 Velocity { get; set; } = Vector3.zero;
        public Vector3 RotAngle { get; set; } = Vector3.zero;
        public Vector3 Forward { get; set; } = Vector3.zero;
        public float TurnSpeed { get; set; } = 0.0f;
        public float SlopeDownLimit { get; set; } = 0.0f;
        public Vector3 Gravity { get; set; } = Vector3.zero;
        public Vector3 AdditionalVelocity { get; set; } = Vector3.zero;
        public Vector3? LookAt { get; set; } = Vector3.zero;
    }

}
