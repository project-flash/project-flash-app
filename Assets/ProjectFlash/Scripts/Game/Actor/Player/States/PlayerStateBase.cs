﻿using Flash.Utility;

namespace Flash
{
    /// <summary>
    /// プレイヤーステートのベースクラス
    /// </summary>
    class PlayerStateBase : State
    {
        protected PlayerStateMachine _stateMachine = null;

        public PlayerStateBase(PlayerStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }
    }
}
