﻿using UnityEngine;

using Flash.Utility;

namespace Flash
{
    //被ダメージ時のステート
    class PlayerState_IsDamaged : PlayerStateBase
    {
        //コンストラクタ
        public PlayerState_IsDamaged(PlayerStateMachine stateMachine) : base(stateMachine) { }
    }
}
