﻿using UnityEngine;

using Flash.Utility;

namespace Flash
{
    /// <summary>
    /// 注目時の処理
    /// </summary>
    class PlayerState_IsObserving : PlayerStateBase
    {
        //コンストラクタ
        public PlayerState_IsObserving(PlayerStateMachine stateMachine) : base(stateMachine) { }

        //アップデート処理
        //OnGroundと比較すると、回転のアップデートをしないだけ
        public override void Execute()
        {
            //速度を入力スピードに更新する
            var velocityXZ = _stateMachine.MoveDirection * _stateMachine.MoveSpeed;
            velocityXZ.y = 0.0f;
            var v = new Vector3(velocityXZ.x, _stateMachine.Velocity.y, velocityXZ.z);

            //地面に沿うように移動ベクトルを修正する処理
            //下りのときのみ補正（地面から浮くのを防止する
            var rayOrigin = _stateMachine.PlayerPosition;
            if (Physics.Raycast(rayOrigin, Vector3.down, out RaycastHit hit))
            {
                //ノーマライズしてから、
                //進行方向ベクトルと法線を外積して、
                //出てきたベクトルと法線を再び外積すると、面に沿ったベクトルが出る
                var nv = v;
                var gn = hit.normal;
                var right = Vector3.Cross(gn, nv);
                var slopeDirection = Vector3.Cross(right, gn).normalized;
                var slopeAngle = 90.0f - Vector3.Angle(Vector3.up, slopeDirection);


                // 斜面を下っている時のみ調整を加える
                if (ExMath.IsRange(slopeAngle, _stateMachine.SlopeDownLimit, 0.0f))
                {
                    v.y = slopeDirection.y * _stateMachine.MoveSpeed;
                }

                Debug.DrawRay(hit.point, hit.normal * 10.0f, Color.red);
                Debug.DrawRay(rayOrigin, slopeDirection * _stateMachine.MoveSpeed, Color.red);
            }

            //速度を更新
            _stateMachine.Velocity = v;
            _stateMachine.RotAngle = Vector3.zero;
        }
    }
}
