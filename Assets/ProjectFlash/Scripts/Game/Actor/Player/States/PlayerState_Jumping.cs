﻿using UnityEngine;

using Flash.Utility;

namespace Flash
{
    // ジャンプ時のステート
    class PlayerState_IsJumping : PlayerStateBase
    {
        //コンストラクタ
        public PlayerState_IsJumping(PlayerStateMachine stateMachine) : base(stateMachine) { }

        //ステートの実行
        public override void Execute()
        {
            var v = _stateMachine.Velocity;

            //重力を加算
            v += _stateMachine.Gravity * Time.deltaTime;
            //追加の速度を計算して加算する
            v += _stateMachine.AdditionalVelocity;

            //出力
            _stateMachine.Velocity = v;
            _stateMachine.RotAngle = Vector3.zero;
        }

    }
}