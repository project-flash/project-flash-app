﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Sirenix.OdinInspector;

using Flash.Utility;

namespace Flash
{
    //接地時のステート
    class PlayerState_OnGround : PlayerStateBase
    {
        //コンストラクタ
        public PlayerState_OnGround(PlayerStateMachine stateMachine) : base(stateMachine) { }

        //ステートの実行
        public override void Execute()
        {
            //速度を入力スピードに更新する
            var velocityXZ = _stateMachine.MoveDirection * _stateMachine.MoveSpeed;
            velocityXZ.y = 0.0f;
            var v = new Vector3(velocityXZ.x, _stateMachine.Velocity.y, velocityXZ.z);

            //向いている方向のアップデート
            var rotAngle = 0.0f;

            //進行方向にモデルを向かせる処理
            var moveSpeed = velocityXZ.magnitude;
            if (moveSpeed > 0.01f)
            {
                rotAngle = -Vector3.SignedAngle(velocityXZ, _stateMachine.Forward, Vector3.up);
            }

            //地面に沿うように移動ベクトルを修正する処理
            //下りのときのみ補正（地面から浮くのを防止する
            var rayOrigin = _stateMachine.PlayerPosition;
            if (Physics.Raycast(rayOrigin, Vector3.down, out RaycastHit hit))
            {
                //ノーマライズしてから、
                //進行方向ベクトルと法線を外積して、
                //出てきたベクトルと法線を再び外積すると、面に沿ったベクトルが出る
                var nv = v;
                var gn = hit.normal;
                var right = Vector3.Cross(gn, nv);
                var slopeDirection = Vector3.Cross(right, gn).normalized;
                var slopeAngle = 90.0f - Vector3.Angle(Vector3.up, slopeDirection);


                // 斜面を下っている時のみ調整を加える
                if (ExMath.IsRange(slopeAngle, _stateMachine.SlopeDownLimit, 0.0f))
                {
                    v.y = slopeDirection.y * _stateMachine.MoveSpeed;
                }

                Debug.DrawRay(hit.point, hit.normal * 10.0f, Color.red);
                Debug.DrawRay(rayOrigin, slopeDirection * _stateMachine.MoveSpeed, Color.red);
            }

            //追加の速度を計算して付与
            v += _stateMachine.AdditionalVelocity;

            _stateMachine.Velocity = v;
            _stateMachine.RotAngle = new Vector3(0.0f, rotAngle * _stateMachine.TurnSpeed * Time.deltaTime, 0.0f);

        }
    }

}