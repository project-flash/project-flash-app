﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Sirenix.OdinInspector;

using Flash.Utility;

namespace Flash
{
    public enum ECameraMode
    {
        FirstPerson,
        ThirdPerson
    }

    public enum EPlayerMode
    {
        Default,
        Camera,
        Battle,
        Swim,
        Radder
    }

    public class PlayerController : SerializedMonoBehaviour
    {
        //イベントセンサー
        [SerializeField]
        private EventCensor _eventCensor = null;

        //プレイヤーモード
        [SerializeField]
        private Dictionary<EPlayerMode, BasePlayerMode> _mode = new Dictionary<EPlayerMode, BasePlayerMode>();

        //操作可能かどうかのフラグ
        public bool IsPlayable { get; private set; } = true;

        //カメラモードの状態
        public ECameraMode CameraMode { get; private set; } = ECameraMode.ThirdPerson;

        //--------------------------
        //内部フィールド
        //--------------------------
        private EPlayerMode _currentMode = EPlayerMode.Default;

        // Start is called before the first frame update
        private void Awake()
        {
        }


        private void Start()
        {
            _mode[_currentMode].Init(this);
        }

        private void FixedUpdate()
        {
            //入力禁止切り替え処理
            if (IsPlayable) _mode[_currentMode].SetInputEnable(true);
            else _mode[_currentMode].SetInputEnable(false);

            _mode[_currentMode].Execute(Time.fixedDeltaTime);
        }

        // Update is called once per frame
        private void Update()
        {
        }

        public void ExecuteEvent()
        {
            IsPlayable = !_eventCensor.RunEvent(() => { IsPlayable = true; });
        }
    }
}