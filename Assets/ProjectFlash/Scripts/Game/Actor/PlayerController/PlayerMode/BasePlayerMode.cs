﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flash
{
    public class BasePlayerMode : MonoBehaviour
    {
        protected PlayerController _controller = null;

        delegate void OnCompleteHandler();
        event OnCompleteHandler OnComplete;

        //入力システム
        protected CustumInput _input = null;


        protected virtual void Awake()
        {
            _input = Game.Instance.InputSystem;
        }

        // 初期化処理
        public virtual void Init(PlayerController controller)
        {
            _controller = controller;
        }

        // CharacterControllerのアップデート内で呼ばれる処理
        public virtual void Execute(float delta)
        {

        }

        public virtual void Exit()
        {
            OnComplete();
        }

        //入力を有効/無効にする
        public virtual void SetInputEnable(bool enable)
        {
        }
    }
}