﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

using Flash.Utility;

namespace Flash
{
    public class BattlePlayerMode : BasePlayerMode, CustumInput.IPlayerBattleActions
    {
        // Start is called before the first frame update
        protected override void Awake()
        {
            base.Awake();
            _input.PlayerBattle.SetCallbacks(this);
        }

        private void OnEnable()
        {
            _input.PlayerBattle.Enable();
        }

        private void OnDisable()
        {
            _input.PlayerBattle.Disable();
        }

        public override void Init(PlayerController controller)
        {
            base.Init(controller);
        }

        public override void Execute(float delta)
        {

        }

        //----------------------------
        //各入力のコールバック
        //----------------------------

        //銃を撃つ処理
        public void OnGunShot(InputAction.CallbackContext context)
        {

        }

        //銃を構える処理
        public void OnGunHold(InputAction.CallbackContext context)
        {

        }

        //着弾点のズーム処理
        public void OnGunZoom(InputAction.CallbackContext context)
        {

        }

        //ローリングアクションの処理
        public void OnRolling(InputAction.CallbackContext context)
        {

        }

        //パンチアクションの処理
        public void OnPunch(InputAction.CallbackContext context)
        {

        }
    }
}