﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

using Flash.Utility;

namespace Flash
{
    public class CameraPlayerMode : BasePlayerMode, CustumInput.IPlayerCameraActions
    {
        [SerializeField]
        private FirstPersonCamera _camera = null;

        [SerializeField]
        private Player _player = null;

        [SerializeField]
        private float _defaultSpeed = 2.0f;

        [SerializeField]
        private float _runSpeed = 3.0f;

        [SerializeField]
        private float _crouchSpeed = 1.0f;

        [SerializeField]
        private float _jumpPower = 5.0f;

        //----------------
        //内部ステート
        //----------------

        //移動速度
        private Vector2 _moveValue = Vector2.zero;
        //走るフラグ
        private bool _isRun = false;
        //カメラの回転量
        private Vector2 _rotCamValue = Vector2.zero;
        


        protected override void Awake()
        {
            base.Awake();
            _input.PlayerCamera.SetCallbacks(this);
        }

        private void OnEnable()
        {
            _input.PlayerSwim.Enable();
        }

        private void OnDisable()
        {
            _input.PlayerSwim.Disable();
        }

        public override void Init(PlayerController controller)
        {
            base.Init(controller);
        }

        public override void Execute(float delta)
        {
            //最大速度をセット
            _player.MaxMoveSpeed = _runSpeed;

            //進む方向を設定
            _player.MoveDirection = Vector3.zero;
            if(_camera != null)
            {
                var camForward = Vector3.Scale(_camera.Forward, new Vector3(1.0f, 0.0f, 1.0f));
                _player.MoveDirection = (_moveValue.y * camForward + _moveValue.x * _camera.Right).normalized;
            }

            //進む速度を設定
            var magnitude = Mathf.Min(new Vector2(_moveValue.x, _moveValue.y).magnitude, 1.0f);
            if(_isRun)
            {
                _player.MoveSpeed = _runSpeed * magnitude;
            }
            else
            {
                _player.MoveSpeed = _defaultSpeed * magnitude;
            }

            //カメラ回転の設定
            _camera.Rotate(_rotCamValue);
        }

        //----------------------------
        //各入力のコールバック
        //----------------------------

        //歩行処理
        public void OnWalk(InputAction.CallbackContext context)
        {
            _moveValue = context.ReadValue<Vector2>();
        }

        //カメラの撮影処理
        public void OnCameraShot(InputAction.CallbackContext context)
        {

        }

        //カメラの回転処理
        public void OnCameraRotation(InputAction.CallbackContext context)
        {
            _rotCamValue = context.ReadValue<Vector2>();
        }

        //カメラのズーム処理
        public void OnZoomIn(InputAction.CallbackContext context)
        {

        }

        //カメラのズームアウト処理
        public void OnZoomOut(InputAction.CallbackContext context)
        {

        }

        //ジャンプ処理
        public void OnJump(InputAction.CallbackContext context)
        {

        }

        //カメラ機能の切り替え処理
        public void OnChangeCamera(InputAction.CallbackContext context)
        {

        }

        //カメラモードの終了処理
        public void OnCameraMode(InputAction.CallbackContext context)
        {

        }
    }
}