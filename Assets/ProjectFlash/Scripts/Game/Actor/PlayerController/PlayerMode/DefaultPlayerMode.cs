﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Sirenix.OdinInspector;
using Flash.Utility;

namespace Flash
{
    public class DefaultPlayerMode : BasePlayerMode, CustumInput.IPlayerDefaultActions
    {
        //プレイヤーにアタッチするカメラ
        [SerializeField]
        private ThirdPersonCamera _thirdpersonCamera = null;

        //プレイヤーにアタッチするプレイヤーコンポーネント
        [SerializeField]
        private Player _player = null;

        //カメラの注視点となるオブジェクト
        [SerializeField]
        private PlayerCameraLookAt _lookAt = null;

        //歩行の最大スピード
        [SerializeField]
        private float _defaultSpeed = 2.0f;

        //ダッシュの最大スピード
        [SerializeField]
        private float _runSpeed = 3.0f;

        //しゃがみ移動時の最大スピード
        [SerializeField]
        private float _crouchSpeed = 1.0f;

        //ジャンプ力
        [SerializeField]
        private float _jumpPower = 5.0f;

        //Z注目時のカメラのオフセット
        [SerializeField]
        private Vector3 _observeCameraOffset = new Vector3(0.3f, 0.0f, -0.8f);

        //Z注目時の注視点までの距離
        [ReadOnly]
        private const float _observeLookatDistance = 3.0f;

        //--------------------------
        //内部フィールド
        //--------------------------
        private Vector2 _moveValue = Vector2.zero;
        private Vector2 _camRotValue = Vector2.zero;
        private bool _isCrouch = false;
        private bool _isRun = false;
        private bool _isObserving = false;

        // Start is called before the first frame update
        protected override void Awake()
        {
            base.Awake();
            _input.PlayerDefault.SetCallbacks(this);


            _thirdpersonCamera.Target = _lookAt.transform;
        }

        public override void Init(PlayerController controller)
        {
            base.Init(controller);

            //TODO ここに初期化処理をかく
            //各ステートのリセット時の値も入れる
        }

        private void OnEnable()
        {
            _input.PlayerDefault.Enable();
        }

        private void OnDisable()
        {
            _input.PlayerDefault.Disable();
        }

        public override void Execute(float delta)
        {
            //最大速度をセット
            _player.MaxMoveSpeed = _runSpeed;

            //進む方向を設定
            _player.MoveDirection = Vector3.zero;
            if (_thirdpersonCamera != null)
            {
                var camForword = Vector3.Scale(_thirdpersonCamera.Forword, new Vector3(1.0f, 0.0f, 1.0f)).normalized;
                _player.MoveDirection = (_moveValue.y * camForword + _moveValue.x * _thirdpersonCamera.Right);
            }

            //進むスピードを設定
            var magnitude = Mathf.Min(new Vector2(_moveValue.x, _moveValue.y).magnitude, 1.0f);
            if (_isCrouch)
            {
                //しゃがみ時のスピードに設定
                _player.MoveSpeed = _crouchSpeed * magnitude;
            }
            else if (_isRun)
            {
                //走り時のスピードに設定
                _player.MoveSpeed = _runSpeed * magnitude;
            }
            else
            {
                //通常のスピードに設定
                _player.MoveSpeed = _defaultSpeed * magnitude;
            }

            //しゃがみ状態の設定
            _player.IsCrouch = _isCrouch;

            //カメラ回転の設定
            _thirdpersonCamera.RotateCenter(_camRotValue.x, _camRotValue.y, delta);

            //Z注目機能
            if (_isObserving)
            {
                var lookat = _lookAt.transform.position + _player.transform.forward * _observeLookatDistance;
                var q = Quaternion.FromToRotation(new Vector3(0.0f, 0.0f, -1.0f), -_player.transform.forward.normalized);
                var pos = q * _observeCameraOffset;
                _thirdpersonCamera.Lock(lookat, pos);
                _player.Lock(null);
            }
        }

        //----------------------------
        //各入力のコールバック
        //----------------------------

        //歩行処理
        public void OnWalk(InputAction.CallbackContext context)
        {
            _moveValue = context.ReadValue<Vector2>();
        }

        //走る処理
        public void OnRun(InputAction.CallbackContext context)
        {
            _isRun = InputUtility.IsPressed(context);
        }

        //しゃがみ状態の処理
        public void OnCrouch(InputAction.CallbackContext context)
        {
            //トグル方式
            _isCrouch = _isCrouch != context.started;
        }

        //Z注目の処理
        public void OnObserve(InputAction.CallbackContext context)
        {
            _isObserving = InputUtility.IsPressed(context);
        }

        //ジャンプ処理
        public void OnJump(InputAction.CallbackContext context)
        {
            _player.Jump(_jumpPower);
        }

        //カメラの回転処理
        public void OnCameraRotation(InputAction.CallbackContext context)
        {
            _camRotValue = context.ReadValue<Vector2>();
        }

        //しらべるボタンの処理
        public void OnIntaract(InputAction.CallbackContext context)
        {
            _controller.ExecuteEvent();
        }

        //カメラモードへの切り替えボタン（トグル）
        public void OnCameraMode(InputAction.CallbackContext context)
        {

        }

        //アイテム使用ボタン
        public void OnUseItem(InputAction.CallbackContext context)
        {

        }

        //カメラ変更ボタン
        public void OnChangeCamera(InputAction.CallbackContext context)
        {

        }

        //武器変更ボタン
        public void OnChangeWeapon(InputAction.CallbackContext context)
        {

        }

        //マップ表示切り替えボタン
        public void OnChangeMap(InputAction.CallbackContext context)
        {

        }
    }
}