﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

using Flash.Utility;

namespace Flash
{
    public class SwimPlayerMode : BasePlayerMode, CustumInput.IPlayerSwimActions
    {
        protected override void Awake()
        {
            base.Awake();
            _input.PlayerSwim.SetCallbacks(this);
        }

        private void OnEnable()
        {
            _input.PlayerSwim.Enable();
        }

        private void OnDisable()
        {
            _input.PlayerSwim.Disable();
        }

        public override void Init(PlayerController controller)
        {
            base.Init(controller);
        }

        public override void Execute(float delta)
        {

        }

        //----------------------------
        //各入力のコールバック
        //----------------------------

        //泳ぐ処理
        public void OnSwim(InputAction.CallbackContext context)
        {

        }

        //早く泳ぐ処理
        public void OnSwimFast(InputAction.CallbackContext context)
        {

        }

        //もぐる処理
        public void OnDive(InputAction.CallbackContext context)
        {

        }

        //ジャンプ処理
        public void OnJump(InputAction.CallbackContext context)
        {

        }

        //調べる処理
        public void OnIntaract(InputAction.CallbackContext context)
        {

        }

        //カメラの回転処理
        public void OnCameraRotation(InputAction.CallbackContext context)
        {

        }

        //Z注目処理
        public void OnObserve(InputAction.CallbackContext context)
        {

        }
    }

}