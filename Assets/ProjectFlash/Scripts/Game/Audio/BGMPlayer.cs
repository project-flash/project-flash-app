﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flash
{
    [RequireComponent(typeof(AudioSource))]
    public class BGMPlayer : MonoBehaviour
    {
        [SerializeField]
        private string _soundName = "";

        [SerializeField]
        private bool _playOnStart = false;

        private AudioSource _audio = null;

        private void Awake()
        {
            _audio = GetComponent<AudioSource>();
        }

        // Start is called before the first frame update
        void Start()
        {
            if (_playOnStart) Play();
        }

        // Update is called once per frame
        public void Play()
        {
            var game = Game.Instance;
            game.SoundManager.Play(_soundName, SoundType.BGM, _audio);
        }
        public void Stop()
        {
            var game = Game.Instance;
            game.SoundManager.Stop(_audio);
        }
    } }
