﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Flash
{
    [RequireComponent(typeof(Button))]
    public class ButtonSound : MonoBehaviour
    {
        [SerializeField]
        private bool _IsOK = true;

        private void Awake()
        {
            var comp = GetComponent<Button>();
            comp.onClick.AddListener(OnClick);
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        private void OnClick()
        {
            Game.Instance.SoundManager.PlaySE((_IsOK) ? SE.OK : SE.Cancel);
        }
    }
}