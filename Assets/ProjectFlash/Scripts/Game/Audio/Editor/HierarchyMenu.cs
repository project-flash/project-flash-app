﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class HierarchyMenu
{
    [MenuItem("GameObject/Flash/BGMSource", false, 0)]
    public static void BGMSource()
    {
        //オブジェクトを生成
        var inst = new GameObject("BGMSource");
        inst.AddComponent<Flash.BGMPlayer>();
        inst.transform.SetParent(Selection.activeTransform);

    }
}
