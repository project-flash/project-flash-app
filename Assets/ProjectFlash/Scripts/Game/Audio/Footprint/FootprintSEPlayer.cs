﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Flash
{
    [RequireComponent(typeof(Rigidbody),typeof(AudioSource), typeof(SphereCollider))]
    public class FootprintSEPlayer : MonoBehaviour
    {
        [SerializeField]
        private List<string> _materialList = new List<string>();

        private AudioSource _audio = null;

        private void Awake()
        {
            _audio = GetComponent<AudioSource>();
        }

        private void OnTriggerEnter(Collider other)
        {
            // 通常のゲームオブジェクトの処理
            var game = Game.Instance;
            var comp = other.GetComponent<SoundMaterial>();
            if (comp != null)
            {
                game.SoundManager.Play(_materialList[comp.FootprintID], SoundType.SE, _audio);
                return;
            }

            // テレインに関する処理
            var compTerrain = other.GetComponent<SoundMaterialTerrain>();
            if (compTerrain != null)
            {
                var t = compTerrain.Terrain;
                var tdata = t.terrainData;
                var relativePos = transform.position - t.transform.position;

                //自分の位置のα値を取得
                var u = (int)(tdata.alphamapWidth * relativePos.x / tdata.size.x);
                var v = (int)(tdata.alphamapHeight * relativePos.z / tdata.size.z);
                var alphamaps = tdata.GetAlphamaps(u, v, 1, 1);

                //alpha値の最大値をその地点の足音として再生
                var weights = alphamaps.Cast<float>().ToArray();
                int layerID = System.Array.IndexOf(weights, weights.Max());

                //SE再生
                var se = compTerrain.FootprintID[layerID];
                game.SoundManager.Play(_materialList[se], SoundType.SE, _audio);
            }
        }
    }
}