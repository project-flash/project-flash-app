﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flash
{
    public class SoundMaterial : MonoBehaviour
    {
        [SerializeField]
        private int _footprintID = 0;
        public int FootprintID { get { return _footprintID; } }
    }
}