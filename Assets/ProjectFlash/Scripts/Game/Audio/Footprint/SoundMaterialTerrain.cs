﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
namespace Flash
{
    [RequireComponent(typeof(Terrain))]
    public class SoundMaterialTerrain : MonoBehaviour
    {
        [SerializeField]
        private int[] _footprintID = { 0 };
        public int[] FootprintID { get { return _footprintID; } }

        public Terrain Terrain { get; set; } = null;

        private void Awake()
        {
            Terrain = GetComponent<Terrain>();
        }
    }
}