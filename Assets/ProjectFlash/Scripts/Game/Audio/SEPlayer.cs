﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flash
{
    public class SEPlayer : MonoBehaviour
    {
        [SerializeField]
        private string _SoundName = "";

        [SerializeField]
        private bool _PlayOnStart = false;

        // Start is called before the first frame update
        void Start()
        {
            if (_PlayOnStart) Play();
        }

        // Update is called once per frame
        public void Play()
        {
            var game = Game.Instance;
            game.SoundManager.Play(_SoundName, SoundType.SE);
        }
        public void Stop()
        {
            var game = Game.Instance;
            game.SoundManager.Stop(SoundType.SE);
        }
    }
}
