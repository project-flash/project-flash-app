﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;

using Sirenix.OdinInspector;

namespace Flash
{
    //サウンドの種類
    public enum SoundType { BGM, SE, Voice }

    //よく使うSE
    public enum SE { OK, Cancel }

    //サウンド管理クラス
    public class SoundManager : SerializedMonoBehaviour
    {

        [SerializeField]
        private AudioSource _BGMIntroSource = null;
        [SerializeField]
        private AudioSource _BGMLoopSource = null;
        [SerializeField]
        private AudioSource _SESource = null;
        [SerializeField]
        private AudioSource _VoiceSource = null;

        //BGMの再生状況、イントロ部分か、ループ部分を再生しているか
        private bool _isPausingBGMIntro = false;
        //BGMの再生名
        private string _playingBGMName = "";


        //よく使うSEの対応表
        [SerializeField]
        private readonly Dictionary<SE, string> _SENameList
            = new Dictionary<SE, string>()
            {
            {SE.OK, "se_00_OK" },
            {SE.Cancel, "se_01_cancel" },
            };

        //--------------------
        // プロパティ
        //--------------------

        //音量
        public float MasterVolume { get; set; } = 1.0f;
        public float BGMVolume { get; set; } = 1.0f;
        public float SEVolume { get; set; } = 1.0f;
        public float VoiceVolume { get; set; } = 1.0f;

        //任意の補正用ボリューム
        private float _adjustVolumeBGM = 1.0f;
        private float _adjustVolumeSE = 1.0f;
        private float _adjustVolumeVoice = 1.0f;

        public void Init()
        {
            //seは予めロードしておく
            var op = Addressables.DownloadDependenciesAsync("se");
        }

        public void Update()
        {
            var game = Game.Instance;

            //ボリュームの同期
            var master = MasterVolume;

            _BGMIntroSource.volume = BGMVolume * _adjustVolumeBGM * master;
            _BGMLoopSource.volume = _BGMIntroSource.volume;

            _SESource.volume = SEVolume * _adjustVolumeSE * master;
            _VoiceSource.volume = VoiceVolume * _adjustVolumeVoice * master;
        }

        //再生開始
        public void Play(string name, SoundType type, float volume = 1.0f, bool isDelay = false)
        {
            Debug.Log("PlaySound :" + name);

            var source = _SESource;
            switch(type)
            {
                case SoundType.BGM:source = _BGMLoopSource;break;
                case SoundType.SE:source = _SESource;break;
                case SoundType.Voice:source = _VoiceSource;break;
            }

            StartCoroutine(_Play(name, "", type, source, null, volume, isDelay));
        }

        //ループBGM再生用のユニークケース
        public void PlayLoopBGM(string intro, string loop)
        {
            Debug.Log("PlayBGM in:" + intro + " lo:" + loop);


            StartCoroutine(_Play(loop, intro, SoundType.BGM, _BGMIntroSource, _BGMLoopSource, 1.0f, true));
        }

        //オーディオソース指定の再生（3Dサウンド用）
        public void Play(string name, SoundType type, AudioSource audiosource, float volume = 1.0f, bool isDelay = false)
        {
            Debug.Log("PlaySound :" + name);

            StartCoroutine(_Play(name, "", type, audiosource, null, volume, isDelay));
        }

        //再生の内部処理（非同期）
        IEnumerator _Play(string clipname, string clipname2, SoundType type, AudioSource source1, AudioSource source2, float volume = 1.0f, bool isDelay = false, bool isExternSource=false)
        {
            //クリップのロード
            AudioClip clip = null;
            AudioClip clip2 = null; //bgmのループ再生用


            //クリップ1を読み出し
            var op = Addressables.LoadAssetAsync<AudioClip>(clipname);
            yield return op;

            if(op.Result==null)
            {
                Debug.LogError("サウンドファイルのロードに失敗しました。：" + clipname);
                yield break;
            }
            clip = op.Result;

            //クリップ2を読み出し
            if(clipname2!="")
            {
                op = Addressables.LoadAssetAsync<AudioClip>(clipname2);
                yield return op;
                if(op.Result==null)
                {
                    Debug.LogError("サウンドファイルのロードに失敗しました。：" + clipname2);
                }
            }
            else
            {
                clip2 = clip;
            }

            //ボリュームの更新
            switch (type)
            {
                case SoundType.BGM: _adjustVolumeBGM = volume; break;
                case SoundType.SE: _adjustVolumeSE = volume; break;
                case SoundType.Voice: _adjustVolumeVoice = volume; break;
            }

            //再生
            switch (type)
            {
                case SoundType.BGM:

                    //同じBGMを再生していた場合はスキップする (外部ソースでない場合)
                    if (!isExternSource)
                    {
                        if (_playingBGMName == clipname) break;
                        _playingBGMName = clipname;
                    }

                    var delay = 1.0f;
                    if (isDelay) delay = 1.0f; //シーンロード用の遅延再生

                    source1.clip = clip2;
                    source1.PlayScheduled(AudioSettings.dspTime + delay);

                    if (source2 != null)
                    {
                        source2.clip = clip;
                        source2.PlayScheduled(AudioSettings.dspTime + delay + ((float)_BGMIntroSource.clip.samples / (float)_BGMIntroSource.clip.frequency));
                    }
                    
                    break;
                case SoundType.SE:
                    source1.PlayOneShot(clip);
                    break;
                case SoundType.Voice:
                    source1.clip = clip;
                    source1.Play();
                    break;

            }

            yield return null;
        }


        //ポーズ(BGM限定) true:ポーズをかける false:ポーズ解除
        public void Pause(bool pause)
        {
            if (pause)
            {
                //イントロ中のポーズかを格納しておく
                _isPausingBGMIntro = _BGMIntroSource.isPlaying;
                _BGMIntroSource.Pause();
                _BGMLoopSource.Pause();
            }
            else
            {
                _BGMIntroSource.UnPause();

                //イントロ中にポーズした場合は、遅延再生を再設定する
                if (_isPausingBGMIntro)
                {
                    _BGMLoopSource.Stop();
                    _BGMLoopSource.PlayScheduled(
                        AudioSettings.dspTime - _BGMIntroSource.time +
                        ((float)_BGMIntroSource.clip.samples / (float)_BGMIntroSource.clip.frequency));
                }
                else
                    _BGMLoopSource.UnPause();
            }

        }

        //再生停止
        public void Stop(SoundType type)
        {
            switch (type)
            {
                case SoundType.BGM:
                    _BGMIntroSource.Stop();
                    _BGMLoopSource.Stop();
                    _BGMIntroSource.clip = null;
                    _BGMLoopSource.clip = null;
                    _playingBGMName = "";
                    break;
                case SoundType.Voice:
                    _VoiceSource.Stop();
                    break;
                case SoundType.SE:
                    _SESource.Stop();
                    break;
            }

        }
        public void Stop(AudioSource source)
        {
            source.Stop();
            source.clip = null;
        }

        //指定のタイプのソースが再生中かどうか
        public bool IsPlaying(SoundType type)
        {
            switch (type)
            {
                case SoundType.BGM:
                    return _BGMIntroSource.isPlaying | _BGMLoopSource.isPlaying;
                case SoundType.SE:
                    return _SESource.isPlaying;
                case SoundType.Voice:
                    return _VoiceSource.isPlaying;
                default:
                    return false;
            }

        }

        //キュー名を取得
        public string GetCueName(SoundType type)
        {
            switch (type)
            {
                case SoundType.BGM: return _playingBGMName;
                case SoundType.SE: return _SESource.clip.name;
                case SoundType.Voice: return _SESource.clip.name;
                default: return "";
            }

        }

        //---------------
        //よく使うSEの専用再生
        //---------------
        public void PlaySE(SE SEType)
        {
            Play(_SENameList[SEType], SoundType.SE);
        }

    }
}