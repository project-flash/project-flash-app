﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Flash
{
    [CreateAssetMenu(fileName ="NewPlayerData", menuName ="Flash/Player")]
    public class PlayerData : BaseData
    {
        //HP
        [SerializeField]
        private int _HP = 5;
        //ダッシュ時の速度
        [SerializeField]
        private float _dashSpeed = 40.0f;
        //通常時の速度
        [SerializeField]
        private float _walkSpeed = 20.0f;
        //ジャンプの高さ
        [SerializeField]
        private float _jumpPower = 10.0f;
        //カメラの残量
        [SerializeField]
        private int _CameraPower = 5;

        //プロパティ
        public int HP { get { return _HP; } }
        public float DashSpeed { get { return _dashSpeed; } }
        public float WalkSpeed { get { return _walkSpeed; } }
        public float JumpPower { get { return _jumpPower; } }
        public int CameraPower { get { return _CameraPower; } }
        
    }
}
