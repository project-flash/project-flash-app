﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using System;
using System.Linq;

using Flash.DebugUtility;
using static Flash.Utility.TypeConverter;

namespace Flash.Data
{
    //CSVデータからデータロードを想定するインターフェース
    public interface ILoadFromCSVFile
    {
        void InitFromCSVData(CSVFile data);
    }
    //任意の配列データをメンバデータに変換するインターフェース
    public interface ILoadFromStringArray
    {
        void InitFromStringList(List<string> data, int index=0);
    }

    //アセットデータのベースクラス
    public class DataAssetBase : ILoadFromStringArray
    {
        public int ID { get; set; }

        public virtual void InitFromStringList(List<string> data, int index = 0) { }
    }

    //アセットデータを配列で使う場合の一般化クラス
    public class DataAssetArray<T> : ILoadFromCSVFile
        where T : DataAssetBase, new()
    {
        public Dictionary<int,T> Data { get; set; } = new Dictionary<int,T>();
        
        //インデクサの定義
        public T this[int index]
        {
            get { return Data[index]; }
            set { Data[index] = value; }
        }

        //CSVからの初期化
        public void InitFromCSVData(CSVFile data)
        {
            //既存のデータの解放
            Data = new Dictionary<int, T>();

            var rowCount = data.GetRowCount();
            for(var i=0; i<rowCount;i++)
            {
                //List<string>から、データを初期化する
                var row = data.GetRow(i);
                var dat = new T();
                dat.InitFromStringList(row, i);
                Data.Add(dat.ID, dat);
            }

        }        
    }

    //ゲーム中で参照するデータすべて
    public class DataAsset : MonoBehaviour
    {
        //各データのリファレンス
        #region DataReference
        
            
        #endregion


        //ローディングステータス
        public bool IsLoading { get; private set; } = false;

        //データを空の状態にリセットする
        public void Clear()
        {
        }

        //データを初期化する
        public void Init()
        {
            Clear();

            StartCoroutine(_Init());
        }

        private IEnumerator _Init()
        {
            IsLoading = true;

            //エリアデータ初期化
            //yield return _LoadDataFromAssetRef(MAreaDataRef, Area);

            IsLoading = false;

            yield return null;
        }

        //アセット参照からデータをロードする
        private IEnumerator _LoadDataFromAssetRef(AssetReference dataRef,ILoadFromCSVFile asset)
        {

            //Addressableアセットからロード
            var ope = Addressables.LoadAssetAsync<TextAsset>(dataRef);
            yield return ope;

            //データ構造を取得
            var csvData = DataLoader.TextToCSV(ope.Result.text);

            //データの格納
            asset.InitFromCSVData(csvData);

            yield return null;
        }
    }

}