﻿using System;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine;

namespace Flash.Data
{
    public class CSVFile
    {
        //データ本体
        private List<List<string>> Data { get; set; } = new List<List<string>>();

        //データの取得
        public string GetCell(int x,int y) { return Data[x][y]; }
        public List<string> GetRow(int row) { return Data[row]; }
        public int GetRowCount() { return Data.Count; }

        //データの追加
        public void AddRow(params string[] dat) { var list = new List<string>(dat); AddRow(list); }
        public void AddRow(List<string> data) { Data.Add(data); }
    }

    public static class DataLoader
    {
        //CSVを読み出す
        public static CSVFile TextToCSV(string data)
        {
            CSVFile outData = new CSVFile();

            //テキストをデータ構造へ変換
            string str = data;
            //CRを削除、CNのみにする
            str = str.Replace("\r", "");

            var rows = str.Split('\n');
            for(int i=1;i<rows.Length; i++) //0はタイトル行として飛ばす
            {
                var column_list = new List<string>();
                var columns = rows[i].Split(',');

                //列が空か、列の先頭が空白の場合はスキップする
                if (columns.Length == 0) continue;
                if (columns[0] == "") continue;

                foreach (var column in columns)
                {
                    column_list.Add(column);
                }
                outData.AddRow(column_list);
            }

            return outData;
        }
    }
}
