﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Flash
{
    //ステージへの入力データ
    public class IN_StageData
    {
    }

    //ノベルシーンへの入力データ
    public class IN_NovelData
    {
        public string ScriptName { get; set; }
        public Action EndCallBack { get; set; }

    }
    //ノベルシーンからの出力
    public class OUT_NovelData
    {

    }

    public class IODataForLevel : MonoBehaviour
    {
        public IN_NovelData In_Novel { get; set; } = new IN_NovelData();
    }
}