﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flash
{
    public class EventCensor : MonoBehaviour
    {

        [SerializeField]
        Player _player = null;

        //発火可能なイベントリスト
        List<EventTrigger> _EventList = new List<EventTrigger>();

        //一番近いイベントを発火
        EventTrigger _NearestEvent = null;

        // Update is called once per frame
        void Update()
        {
            //最短距離のイベントを更新する。
            //あんまり毎回やると重いので、5フレームに1回の更新とする
            if(Time.frameCount % 5 == 0)
            {
                UpdateNearestEvent();
            }
            if (_NearestEvent != null) 
                Debug.DrawLine(transform.position, _NearestEvent.transform.position + new Vector3(0.0f, 1.0f, 0.0f), Color.yellow);

        }

        //最も近いイベントを求める
        void UpdateNearestEvent()
        {
            float _MinDist = 999.0f;
            _NearestEvent = null;
            foreach (var e in _EventList)
            {
                var dist = (transform.position - e.transform.position).magnitude;
                if (_MinDist > dist)
                {
                    _MinDist = dist;
                    _NearestEvent = e;
                }
            }
        }

        //一番近いイベントを発火する
        //返り値は成功判定
        public bool RunEvent(System.Action onComplete)
        {
            if (_NearestEvent != null)
            {
                //イベントの方を向かせる
                _player.TurnTo(_NearestEvent.transform.position);

                //イベント発火
                _NearestEvent.RunEvent(this, onComplete);

                return true;
            }

            return false;
        }

        //イベントリストの更新
        public void OnTriggerEnter(Collider collider)
        {
            var comp = collider.GetComponent<EventTrigger>();
            if (comp!=null)
            {
                _EventList.Add(comp);
            }
        }
        public void OnTriggerExit(Collider collider)
        {
            var comp = collider.GetComponent<EventTrigger>();
            if(comp!=null)
            {
                _EventList.Remove(comp);
            }
        }
    }
}