﻿using Koromosoft.Novel;
using UnityEngine.InputSystem;

using Flash.Utility;

namespace Flash
{
    class EventInputController : NovelInputController, CustumInput.IEventActions
    {
        private CustumInput _input = null;

        void Awake()
        {
            _input = Game.Instance.InputSystem;
            _input.Event.SetCallbacks(this);
        }

        void OnEnable()
        {
            _input.Event.Enable();
        }

        void OnDisable()
        {
            _input.Event.Disable();
        }

        //クリック処理
        public void OnEnter(InputAction.CallbackContext context)
        {
            _engine.SS_Text.Click();
        }

        //スキップ処理
        public void OnSkip(InputAction.CallbackContext context)
        {
            _engine.SS_Text.SetSkip(InputUtility.IsPressed(context));
        }

        //オートボタン処理
        public void OnAuto(InputAction.CallbackContext context)
        {
            //TODO: Autoモードの実装
        }
    }
}
