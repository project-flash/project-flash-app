﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Flash
{
    [RequireComponent(typeof(Pawn))]
    public class EventTrigger : MonoBehaviour
    {
        [SerializeField]
        private string _ScriptPath = "";

        private Pawn _Pawn = null;

        private void Awake()
        {
            _Pawn = GetComponent<Pawn>();
        }

        public void RunEvent(EventCensor e, Action callback)
        {
            //NPCを振り向かせる
            _Pawn.TurnTo(e.transform.position);

            var game = Game.Instance;
            game.Commands.RunNovel(_ScriptPath, callback);
        }
    }
}