﻿using UnityEngine;
using UnityEngine.SceneManagement;

using Koromosoft.Novel;

namespace Flash
{
    class NovelSceneController : BaseSceneController
    {
        [SerializeField]
        private NovelEngine _NovelEngine=null;
        [SerializeField]
        private string _ScriptFolderPath = "NvScripts/";
        private void Start()
        {
            var game = Game.Instance;
            var inputData = game.IODataForLevel.In_Novel;

            //ノベルスタート
            _NovelEngine.EndCallBack += inputData.EndCallBack;
            _NovelEngine.EndCallBack += () => { SceneManager.UnloadSceneAsync("Novel"); };
            _NovelEngine.Execute(_ScriptFolderPath + inputData.ScriptName);
        }
    }
}
