﻿#define LANG_JP//日本語

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K3
{
    public enum ELevel : int
    {
        Start,
        Title,
        Area,
        _Num
    }

    //enum->stringのための拡張メソッド
    public static class ELevelEx
    {
        public static string GetStr(this ELevel value)
        {
            string[] str =
            {
                "Start",
                "Title",
                "Area"
            };

            return str[(int)value];
        }
    }

    //ゲーム中に登場する定数
    public static class GameConst
    {
        //ステージの１マス当たりのサイズ
        public static readonly int StageUnitSize = 3;

        //戦闘メンバーの人数
        public static readonly int BattleFrontMemberNum = 4;
        //待機人数を合わせたパーティメンバーの最大人数
        public static readonly int BattleMemberNum = 12;
        //セットできる戦闘スキルの最大数
        public static readonly int MaxCurrentBattleSkillNum = 3;
        //セットできる補助スキルの最大数
        public static readonly int MaxCurrentSubSkillNum = 5;

        //装備スロット数
        public static readonly int UnitEquipmentSlotNum = 5;

        //言語対応用
#if LANG_JP
        //前衛、中衛、後衛の名称
        public static readonly string[] PositionName = { "前衛", "中衛", "後衛" };
#endif

    }

    //ルートタイプ
    public enum ERouteType
    {
        Defualt,
        Rare,
        _Num
    }

    //ステージタイプ
    public enum EStageType
    {
        Default,
        Special,
        _Num
    }

    //ユニットのポジションデータ
    public enum EUnitPosition : int
    {
        Front,
        Middle,//敵のみ使用
        Back,
        _Num
    }


    //耐性定義
    public enum EBattleResist : int
    {
        Fire_Ice,   //炎氷
        Earth_Wind, //土風
        Light_Dark, //光闇
        Poison,     //毒
        Pararise,   //麻痺
        Stun,       //スタン
        Death,      //死
        Bf_PAtk,    //物理攻撃　デバフ
        Bf_MAtk,    //魔法攻撃　デバフ
        Bf_PDef,    //物理防御　デバフ
        Bf_MDef,    //魔法防御　デバフ
        Bf_Weight,  //重量　デバフ
        AP,         //APへらし
        _Num
    }

    //スキルタイプ
    public enum ESkillType
    {
        Attack,         //攻撃
        Support_Buf,    //補助スキル　バフデバフ系
        Support_Stat,   //補助スキル　ステータス系
        Cure_BufStat,       //回復スキル　バフデバフ、ステータス系
        Cure_HPAP,      //回復スキル　HP,AP系
        _Num
    }

    //スキルの効果範囲
    public enum ESkillTargetRange
    {
        All,    //全体
        Group,  //列
        Unit,   //ユニット
        _Num
    }

    //スキルの効果対象
    public enum ESkillTarget
    {
        ToMine,     //自分自身
        ToFriend,   //味方側
        ToEnemy,    //敵側
        _Num
    }



}