﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using DG.Tweening;

using Flash.Data;
using Flash.Utility;


namespace Flash
{
    public class Game : MonoBehaviour
    {
        //システムコマンド
        [SerializeField]
        private SystemCommands _SystemCommands = null;
        public SystemCommands Commands { get { return _SystemCommands; } }

        //ゲームデータ
        [SerializeField]
        private DataAsset _DataAsset = null;
        public DataAsset Data { get { return _DataAsset; } set { _DataAsset = value; } }

        //レベル間データ
        [SerializeField]
        private IODataForLevel _IODataForLevel = null;
        public IODataForLevel IODataForLevel { get { return _IODataForLevel; } set { _IODataForLevel = value; } }

        //サウンド
        [SerializeField]
        private SoundManager _SoundManager = null;
        public SoundManager SoundManager { get { return _SoundManager; } }

        public CustumInput InputSystem { get; private set; }

        //シングルトンの実装
        private static Game mInst;
        public static Game Instance
        {
            get
            {
                if (mInst == null)
                {
                    GameObject gameObject = Resources.Load("Prefabs/Game") as GameObject;
                    mInst = Instantiate(gameObject).GetComponent<Game>();
                }
                return mInst;
            } 
        }

        void Awake()
        {
            DontDestroyOnLoad(this.gameObject);

            if (mInst == null)
            {
                this.Setup();
                mInst = this;
            }
            else if (mInst != this)
            {
                Destroy(this.gameObject);
            }
        }

        //初期化処理
        void Setup()
        {
            //入力の初期化
            InputSystem = new CustumInput();
            InputSystem.Enable();

            //サウンドの初期化
            _SoundManager.Init();

            //データの初期化
            InitDataLoad();
            
        }

        private void OnDestroy()
        {
            DOTween.KillAll();
        }

        //データの初期化
        public void InitDataLoad()
        {
            //現在のデータを初期化
            Data.Init();
        }

        public void Update()
        {
            if(InputUtility.IsPressed(InputSystem.System.ExitApp))
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            }
        }
    }
}