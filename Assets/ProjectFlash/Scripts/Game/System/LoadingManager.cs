﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingManager : MonoBehaviour {

    [SerializeField]
    private Animator[] MAnimator = new Animator[0];

    [SerializeField]
    private float MEffectDuration = 1.0f;

	public IEnumerator End()
    {
        foreach(var anim in MAnimator)
        {
            anim.SetTrigger("End");
        }
        yield return null;

        yield return new WaitForSeconds(MEffectDuration);
    }
}
