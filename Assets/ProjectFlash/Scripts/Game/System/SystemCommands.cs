﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Flash
{
    /// <summary>
    /// どのシーンからもGameクラスを介して呼べるシステムコマンド
    /// アプリを終了したり、レベルを遷移したり、といった、
    /// ゲームにおける全体的制御を担当する
    /// </summary>
    public class SystemCommands : MonoBehaviour
    {
        [SerializeField]
        private GameObject MLoadingWindow = null;

        // 各レベルのマネージャー
        //public Area.AreaManager Area { get; set; }
        //public StageSelect.StageSelectManager StageSelect { get; set; }
        //public Stage.StageManager Stage { get; set; }

        //ロード画面付きのロードを行う
        public void LoadSceneAsyncWithLoading(string levelName)
        {
            //シングルトンがStartCoroutineしないと、シーン読み込み時にプロセスが中断してしまう。
            StartCoroutine(_LoadSceneAsyncWithLoading(levelName));
        }

        private IEnumerator _LoadSceneAsyncWithLoading(string levelName)
        {
            //ロード画面を表示する
            var inst = Instantiate(MLoadingWindow, this.transform);
            yield return new WaitForSeconds(0.15f); //ロード画面がでるまで少しまつ

            //本画面をロードする
            yield return SceneManager.LoadSceneAsync(levelName);

            //ローディング画面の終了アニメーションを表示する
            yield return inst.GetComponent<LoadingManager>().End();

            //ロード画面を消去する
            Destroy(inst);
        }

        //会話システムを起動する
        public void RunNovel(string scriptPath, Action callback)
        {
            var game = Game.Instance;

            //すでにノベルが走っている場合はキャンセル
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                if (SceneManager.GetSceneAt(i).name == "Novel")
                {
                    Debug.LogWarning("[Novel Cancel]" + scriptPath);
                    callback();
                    return;
                }
            }

            //シーン間データをセット
            game.IODataForLevel.In_Novel.ScriptName = scriptPath;
            game.IODataForLevel.In_Novel.EndCallBack += callback;

            //ノベルシーンを開始
            SceneManager.LoadScene("Novel", LoadSceneMode.Additive);

        }
    }
}