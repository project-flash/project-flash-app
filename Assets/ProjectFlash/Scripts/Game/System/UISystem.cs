﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Flash
{
    public class UISystem : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        //UIをロードする
        //二重ロードはシない
        private void LoadUI(string name)
        {
        }

        //既存のUIの上にUIを重ねる
        private void AddUI(string name)
        {

        }

        //一番上のUIを削除する
        private void RemoveUI()
        {

        }

        //任意のUIを削除する
        private void RemoveUI(string name)
        {

        }

        //すべてのUIをアンロードする
        private void UnloadUI()
        {

        }
    }
}