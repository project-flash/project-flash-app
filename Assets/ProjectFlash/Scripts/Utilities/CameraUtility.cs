﻿using UnityEngine;

namespace Flash.Utility
{
    class CameraUtility
    {
        //カメラのニアプレーンのサイズを計算して返します。
        //fovはverticalである必要があります。
        public static Vector2 GetNearplaneSize(Camera cam)
        {
            var size = new Vector2();

            var fovy = cam.fieldOfView;
            var aspect = cam.aspect;
            var np = cam.nearClipPlane;

            size.y = 2 * np * Mathf.Tan(Mathf.Deg2Rad * fovy/2.0f);
            size.x = size.y * aspect;

            return size;
        }
    }
}
