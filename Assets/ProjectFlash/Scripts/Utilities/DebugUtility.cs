﻿using UnityEngine;

namespace Flash.DebugUtility
{
    static class GameDebug
    {
        public static void Log(string message)
        {
            var str = "【Log】" + message;
            Debug.Log(str);

            //画面上にもエラーメッセージを出す拡張をする
            var game = Game.Instance;
        }

        public static void Error(string message)
        {
            var str = "【Error】" + message;
            Debug.LogError(str);

            //画面上にもエラーメッセージを出す拡張をする
            var game = Game.Instance;
        }
    }
}
