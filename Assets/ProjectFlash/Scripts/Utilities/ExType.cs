﻿using UnityEngine;

namespace Flash
{
    public class IntVector2
    {
        public IntVector2() { }
        public IntVector2(int _x, int _y) { x = _x;y = _y; }

        public int x { get; set; } = 0;
        public int y { get; set; } = 0;

        public static IntVector2 operator + (IntVector2 val1, IntVector2 val2)
        {
            return new IntVector2(val1.x + val2.x, val1.y + val2.y);
        }

        public static IntVector2 operator * (IntVector2 val1, int val2)
        {
            return new IntVector2(val1.x * val2, val1.y * val2);
        }

        public static explicit operator Vector2(IntVector2 vec)
        {
            return new Vector2(vec.x, vec.y);
        }
    }
}
