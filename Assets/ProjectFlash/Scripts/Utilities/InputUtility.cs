﻿using UnityEngine.InputSystem;

namespace Flash.Utility
{
    public static class InputUtility
    {
        public static bool IsPressed(InputAction.CallbackContext context)
        {
            return context.ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint;
        }

        public static bool IsPressed(InputAction action)
        {
            return action.ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint;
        }
    }
}
