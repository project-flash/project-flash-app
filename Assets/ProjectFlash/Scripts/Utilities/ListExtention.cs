﻿using System.Collections.Generic;

namespace Flash
{
    /// <summary>
    /// Listコンテナの拡張メソッド
    /// </summary>
    public static class ListExtention
    {
        /// <summary>
        /// 最後の要素を取得して返す
        /// </summary>
        public static T GetLast<T>(this List<T> list)
        {
            return list[list.Count - 1];
        }
    }
}
