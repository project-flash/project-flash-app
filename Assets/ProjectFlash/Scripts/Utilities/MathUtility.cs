﻿using System;
using UnityEngine;

namespace Flash.Utility
{
    static class ExMath
    {
        public static bool IsRange<T>(T src, T min, T max) where T : IComparable
        {
            return src.CompareTo(min) >= 0 && src.CompareTo(max) <= 0;
        }
    }

    //正規分布の乱数を生成
    public static class NormalRandom
    {
        //N(0.5,1)の正規分布を取得
        public static float value
        {
            get
            {
                return GetValue(0.5f, 1);
            }
        }

        //N(μ,σ^2)の正規分布を取得
        public static float GetValue(float mue, float omega)
        {
            //一様乱数を生成
            var a = UnityEngine.Random.value;
            var b = UnityEngine.Random.value;

            //ボックスミュラー法で正規分布へ変換
            return Mathf.Sqrt(-2 * Mathf.Log(a)) * Mathf.Sin(2 * Mathf.PI * b) * omega + mue;
        }
    }
}
