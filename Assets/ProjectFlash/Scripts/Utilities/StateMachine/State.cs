﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flash.Utility
{
    /// <summary>
    /// ステートのベースクラス
    /// </summary>
    class State
    {
        /// <summary>
        /// 初期化。前のステートの終了処理の直後に走る
        /// </summary>
        /// <param name="stateMachine"></param>
        public virtual void Init()
        {
        }

        /// <summary>
        /// 実行処理。毎フレーム呼ばれる処理を書く
        /// </summary>
        public virtual void Execute() { }

        /// <summary>
        /// 終了処理。次のステートの初期化の前に走る
        /// </summary>
        public virtual void End() { }
    }
}
