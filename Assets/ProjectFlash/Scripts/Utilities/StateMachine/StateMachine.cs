﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flash.Utility
{
    /// <summary>
    /// ステートを統括するステートマシンのベースクラス
    /// </summary>
    class StateMachine
    {
        //遷移処理データ用の構造体
        protected struct Transition
        {
            public Transition(int _from, int _to, Func<bool> _transition)
            {
                from = _from; to = _to; transition = _transition;
            }

            //遷移元ステート
            public int from;

            //遷移先ステート
            public int to;

            //遷移条件
            public Func<bool> transition;
        }

        //ステートリスト
        protected Dictionary<int, State> _states = new Dictionary<int, State>();

        //遷移リスト
        protected List<Transition> _transitions = new List<Transition>();

        //現在のステート
        protected int _currentState = -1;

        //現在のステートからのトランジションリスト
        protected List<Transition> _currentTransitions = new List<Transition>();

        //ステートを追加する
        public void AddState(int id, State state)
        {
            _states.Add(id, state);
        }

        //遷移条件を追加する
        public void AddTransition(int from, int to, Func<bool> transition)
        {
            _transitions.Add(new Transition { from = from, to = to, transition = transition });
        }

        //初期化処理
        public void Init(int initialState=0)
        {
            ChangeState(initialState);
        }

        //アップデート処理
        public void Update()
        {
            //ステートの更新
            foreach(var t in _currentTransitions)
            {
                if(t.transition())
                {
                    ChangeState(t.to);
                    break;
                }
            }

            //ステートの実行処理
            _states[_currentState].Execute();
        }

        //終了処理
        public void Shutdown()
        {
            _states[_currentState].End();
        }

        //ステートの遷移処理
        private void ChangeState(int to)
        {
            if(_currentState!=-1) _states[_currentState].End();
            _states[to].Init();
            _currentState = to;

            //現在のステートから伸びる遷移をすべて取得してリスト化
            _currentTransitions = new List<Transition>();
            foreach (var t in _transitions)
                if (t.from == _currentState) _currentTransitions.Add(t);
        }

    }
}
