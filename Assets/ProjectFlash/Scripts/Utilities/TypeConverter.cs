﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Flash.DebugUtility;

namespace Flash.Utility
{
    //型変換機
    static class TypeConverter
    {
        //string→intへの変換
        public static int ToInt(string str)
        {
            int value = 0;
            if(!int.TryParse(str, out value))
            {
                GameDebug.Log("Int型の変換に失敗しました。 Data:" + str);
                return 0;
            }
            return value;
        }

        //スペース区切りstring→intリストへの変換
        public static List<int> ToIntList(string str)
        {
            var strList = str.Split(' ');
            List<int> outList = new List<int>();
            foreach(var s in strList)
            {
                if (s == "") continue;
                outList.Add(ToInt(s));
            }
            return outList;
        }
    }
}
